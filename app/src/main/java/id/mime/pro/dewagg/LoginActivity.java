package id.mime.pro.dewagg;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.livechatinc.inappchat.ChatWindowActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import id.mime.pro.dewagg.storage.DB;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static id.mime.pro.dewagg.MainActivity.IS_RUNNING;
import static id.mime.pro.dewagg.MainActivity.LOGINAUTH;
import static id.mime.pro.dewagg.MainActivity.PACKAGE;
import static id.mime.pro.dewagg.MainActivity.PACKAGENAME;
import static id.mime.pro.dewagg.MainActivity.PACKAGENUMBER;
import static id.mime.pro.dewagg.MainActivity.UID;


public class LoginActivity extends AppCompatActivity {

    private Button SignIn;
    private TextView Contact;
    private TextView Register;
    private EditText UserLogin;
    private EditText PassLogin;
    private String username;
    private RelativeLayout login_panel;
    private RelativeLayout login_loading;
    private String urlinpos;
//    private TextView loginfp;
    private TextView txtForgetPass;
    private CheckBox checkShowPass;

    private String liveChatLicense;
    private String registerUrl;
    private String forgetPassUrl;
    private String userAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_activity);

        Bundle Login = getIntent().getExtras();
        if (Login != null) {
            urlinpos = Login.getString("urlinpos");

        }

        login_panel = (RelativeLayout) findViewById(R.id.login_panel);
        login_loading = (RelativeLayout) findViewById(R.id.login_loading);
        login_panel.setVisibility(View.VISIBLE);
        login_loading.setVisibility(View.GONE);
//        loginfp = (TextView) findViewById(R.id.loginfp);
        checkShowPass = findViewById(R.id.checkbox_show_pass);

        JSONObject urlObj = null;
        try {
            urlObj = new getUrl().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (urlObj != null) {
            try {
                liveChatLicense = urlObj.getString("livechat_license");
                registerUrl = urlObj.getString("register");
                forgetPassUrl = urlObj.getString("forgetpass");
                userAgent = urlObj.getString("user_agent");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SharedPreferences pref = getApplicationContext().getSharedPreferences("external_data", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("live_chat_license", liveChatLicense);
        editor.putString("user_agent", userAgent);
        editor.apply();

        /*FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());
        Log.e("is", String.valueOf(fingerprintManager.isHardwareDetected()));
        if (fingerprintManager.isHardwareDetected()) {
            loginfp.setVisibility(View.VISIBLE);
        }  else {
            loginfp.setVisibility(View.GONE);
        }*/

//        loginfp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LoginActivity.this, FingerPrintActivity.class);
//                startActivity(intent);
//            }
//        });
        //DEFINE LAYOUT

        login_panel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(LoginActivity.this.getCurrentFocus().getWindowToken(), 0);
            }
        });

        SignIn = (Button) findViewById(R.id.SignInButton);
        UserLogin = (EditText) findViewById(R.id.UserLogin);
        PassLogin = (EditText) findViewById(R.id.PassLogin);

        username = checkUsername();
        if (Objects.equals(username, "0")) {
            username = "";
        } else {
            EditText usepass = (EditText) findViewById(R.id.PassLogin);
            usepass.setText("");
        }
        UserLogin.setText(username);

        SignIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Objects.equals(UserLogin.getText().toString(), "")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("USER ID TIDAK BOLEH KOSONG")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    if (Objects.equals(PassLogin.getText().toString(), "")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setMessage("PASSWORD TIDAK BOLEH KOSONG")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        login_loading.setVisibility(View.VISIBLE);
                        login_panel.setVisibility(View.GONE);
                        View view = LoginActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                LoginFunc();
                            }
                        }, 2000);
                    }
                }
            }
        });

        PassLogin.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press

                    if (Objects.equals(UserLogin.getText().toString(), "")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setMessage("USER ID TIDAK BOLEH KOSONG")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        if (Objects.equals(PassLogin.getText().toString(), "")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage("PASSWORD TIDAK BOLEH KOSONG")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            login_loading.setVisibility(View.VISIBLE);
                            login_panel.setVisibility(View.GONE);
                            View view = LoginActivity.this.getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    LoginFunc();
                                }
                            }, 2000);
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        Contact = (TextView) findViewById(R.id.ContactSupport);
        Contact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent chat = new Intent(LoginActivity.this, ChatWindowActivity.class);

                chat.putExtra(ChatWindowActivity.KEY_GROUP_ID, "0");
                chat.putExtra(ChatWindowActivity.KEY_LICENCE_NUMBER, liveChatLicense);
                chat.putExtra(ChatWindowActivity.KEY_VISITOR_NAME, username);
                chat.putExtra("DewaGG", "Android App");
                startActivity(chat);
            }
        });

        Register = (TextView) findViewById(R.id.RegisterButton);
        Register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent x = new Intent(LoginActivity.this, SingleBrowser.class);
                x.putExtra("url", registerUrl);
                startActivityForResult(x, 0);
            }
        });

        checkShowPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked)
                    PassLogin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                else
                    PassLogin.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });


        txtForgetPass = (TextView) findViewById(R.id.txt_forget_pass);
        txtForgetPass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SingleBrowser.class);
                intent.putExtra("url", forgetPassUrl);
                startActivity(intent);
            }
        });
    }

    private String checkUsername() {
        SharedPreferences settings = getSharedPreferences(LOGINAUTH, MODE_PRIVATE);
        return settings.getString("USERNAME", "0");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void LoginFunc() {

        View view = LoginActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        Editable User = UserLogin.getText();
        Editable Pass = PassLogin.getText();
        login_loading.setVisibility(View.VISIBLE);
        String[] userdata = new String[6];
        userdata[0] = PACKAGENUMBER;
        userdata[1] = PACKAGENAME;
        userdata[2] = PACKAGE;
        userdata[3] = String.valueOf(User);
        userdata[4] = String.valueOf(Pass);
        userdata[5] = UID;

        Connection ConnExt = new Connection();
        String Config = null;
        while (Config == null) {
            try {
                Config = ConnExt.getConfiguration(userdata);
                if (Config != null) {

                }
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.d("CONFIGURATION", Config);


        if (Objects.equals(Config, "0")) {
            login_panel.setVisibility(View.VISIBLE);
            login_loading.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("USERNAME ATAU PASSWORD TIDAK COCOK")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else if (Objects.equals(Config, "3")) {
            login_panel.setVisibility(View.VISIBLE);
            login_loading.setVisibility(View.GONE);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("DEWAGG SEDANG MAINTENANCE MOHON DICOBA SESAAT LAGI")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else {

            //SUCCESS
            try {
                CreateDB(String.valueOf(User));
            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent Browser = new Intent(LoginActivity.this, BrowseActivity.class);
            Browser.putExtra("loginprompt", Config);
            Browser.putExtra("Username", String.valueOf(User));
            Browser.putExtra("Password", String.valueOf(Pass));
            LoginAct(String.valueOf(User));
            UpdateLogix(String.valueOf(User));
            finish();
            startActivityForResult(Browser, 0);
            overridePendingTransition(0, 0);
            //SUCCESS
        }
    }

    private void CreateDB(String user) throws Exception {
        DB database = new DB(this);
        database.CreateNotifDb(getApplicationContext(), user);
    }

    private void LoginAct(String userx) {
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "false");
        editor.apply();
    }

    private void UpdateLogix(String user) {
        SharedPreferences LoginAuth;
        LoginAuth = getSharedPreferences(LOGINAUTH, MODE_PRIVATE);
        SharedPreferences.Editor editor = LoginAuth.edit();
        editor.putString("USERNAME", user);
        editor.apply();
        EditText useloggin = (EditText) findViewById(R.id.UserLogin);
        username = checkUsername();
        if (Objects.equals(username, "0")) {
            username = "";
        } else {
            EditText usepass = (EditText) findViewById(R.id.PassLogin);
            usepass.setText("");
            usepass.requestFocus();
        }
        useloggin.setText(username);
    }

    @Override
    public void onBackPressed() {
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "false");
        editor.apply();
        finish();
    }

    private static class getUrl extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            OkHttpClient okHttpClient = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.URL_ENDPOINT)
                    .build();

            JSONObject obj = null;
            try (Response response = okHttpClient.newCall(request).execute()) {
                obj = new JSONObject(response.body().string());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        public void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
        }
    }

}
