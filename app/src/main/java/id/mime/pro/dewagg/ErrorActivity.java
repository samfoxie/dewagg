package id.mime.pro.dewagg;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Objects;

/**
 * Created by pc on 5/19/2017.
 */

public class ErrorActivity extends AppCompatActivity {

    private String ERROR_MESSAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle error = getIntent().getExtras();
        if(error != null){
            String ErrorType = error.getString("ERROR");
            if(Objects.equals(ErrorType, "CONNECTION")){
                ERROR_MESSAGE = "TIDAK BISA TERHUBUNG KE INTERNET";
            }
            else {
                ERROR_MESSAGE = "202 FAILED TO CONNECT";
            }
        }
        else {
            ERROR_MESSAGE = "201 FAILED TO CONNECT";
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.error_activity);
        TextView ErrorMessage = (TextView) findViewById(R.id.error_message);
        ErrorMessage.setText(ERROR_MESSAGE);
    }
}