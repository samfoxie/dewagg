package id.mime.pro.dewagg;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Message;

import com.google.android.material.snackbar.Snackbar;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableWebView;
import com.livechatinc.inappchat.ChatWindowActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import id.mime.pro.dewagg.fragment.ListMessageBoxAdapter;
import id.mime.pro.dewagg.fragment.MessageBoxSingle;
import id.mime.pro.dewagg.google.FireBaseMessaging;
import id.mime.pro.dewagg.storage.DB;
import id.mime.pro.dewagg.storage.DataModel;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static id.mime.pro.dewagg.MainActivity.IS_RUNNING;
import static id.mime.pro.dewagg.MainActivity.NOTIF_PERMISSION;
import static id.mime.pro.dewagg.MainActivity.ulrdmn;
import static id.mime.pro.dewagg.MainActivity.urlinpos;

public class BrowseActivity extends AppCompatActivity {

    public static String CURRENTLY_ACTIVE = "false";
    Context context;
    static BrowseActivity ba;
    public static int mCount;
    private ObservableWebView mWebView;
    private int isLogin = 0;
    private RelativeLayout overlay;
    private SwipeRefreshLayout swipe;
    private int isLogout = 0;
    private String loginprompt;
    private String Username;

    private String NotifPermission;
    private Switch notification_switch;
    private TextView notif_status;
    private ImageView notif_button, setting_button;

    private static ListMessageBoxAdapter adapter;
    private List<String[]> MessageNotif;
    private ArrayList MessageArray;
    private ListView notif_list;

    private MessageReceiver MessageReceiver;

    private int Menuopened = 0;
    private int Settingopened = 0;

    private RelativeLayout messagebox, settingbox;
    private Switch onOffSwitch;

    private PackageInfo packageInfo;

    private String CURRENT_HOST;
    private String[] satuandmn;

    private int login_success = 0;

    private int isLoading = 0;
    private RelativeLayout loadingover;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private String configL;

    private String jsLogin;

    private String jsLogout;

    private OnScrollChangedCallback mOnScrollChangedCallback;
    private static ArrayList<DataModel> dataModels;
    public static Cursor c;
    private Cursor d;
    private ObservableWebView webView;
    private Toolbar toolbar;
    private static ProgressBar notif_loading;
    private static TextView NoMessage;
    private TextView notif_count;
    private static TextView loadmore;
    private TextView removeAll;
    private String Password;
    private TextView readAll;

    public OnScrollChangedCallback getOnScrollChangedCallback() {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    private ValueCallback<Uri[]> filePathCallback;
    private ValueCallback<Uri> filePathCallbackLegacy;

    private ActivityResultLauncher<Intent> fileChooserLauncher;
    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    public static interface OnScrollChangedCallback {
        public void onScroll(int l, int t);
    }

    protected void onCreate(Bundle savedInstanceState) {

        context = this;
        ba = BrowseActivity.this;


        final String isCliked = getIntent().getStringExtra("IsCliked");


        packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        configL = null;
        try {
            configL = new GetConfL().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        int try_connect = 0;
        while (configL == null) {
            try_connect = try_connect + 1;
            if (try_connect > 5) {
                //NO CONNECTION
                Intent error = new Intent(this, ErrorActivity.class);
                error.putExtra("ERROR", "CONNECTION");
                startActivity(error);
                //NO CONNECTION
            } else {
                try {
                    configL = new GetConfL().execute().get();
                } catch (InterruptedException | ExecutionException ignored) {

                }
            }
        }


//        Log.d("CONFIGURATION",configL);

        try {
            JSONObject ConfigObject = new JSONObject(configL);
            jsLogin = ConfigObject.getString("login");
            jsLogout = ConfigObject.getString("logout");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "true");
        editor.apply();

        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.browse_activity);

        CURRENTLY_ACTIVE = "true";

        webView = (ObservableWebView) findViewById(R.id.webView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        notif_loading = (ProgressBar) findViewById(R.id.notif_loading);
        NoMessage = (TextView) findViewById(R.id.NoMessage);
        notif_count = (TextView) findViewById(R.id.notif_count);
        loadmore = (TextView) findViewById(R.id.load_more);
        removeAll = (TextView) findViewById(R.id.removeAll);
        readAll = (TextView) findViewById(R.id.readAll);

        removeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataModels == null || dataModels.isEmpty()) {
                    Toast.makeText(context, "No notification to remove!", Toast.LENGTH_SHORT).show();
                } else {
                    DB dataBaseHelper = new DB(context);
                    dataBaseHelper.openDatabase();
                    dataBaseHelper.deleteAll();
                    Toast.makeText(context, "Successfully removed all notification!", Toast.LENGTH_SHORT).show();
                    dataBaseHelper.close();
                    Loaddb();
                }

            }
        });

        readAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataModels == null || dataModels.isEmpty()) {
                    Toast.makeText(context, "No notification to mark as read!", Toast.LENGTH_SHORT).show();
                } else {
                    DB dataBaseHelper = new DB(context);
                    dataBaseHelper.openDatabase();
                    dataBaseHelper.readallnotification("true");
                    Toast.makeText(context, "Successfully marked all notification as read!", Toast.LENGTH_SHORT).show();
                    dataBaseHelper.close();
                    Loaddb();
                }

            }
        });

        isLogin = 0;
        isLogout = 0;
        mWebView = null;

        Bundle Browser = getIntent().getExtras();

        if (Browser != null) {
            loginprompt = Browser.getString("loginprompt");
            Username = Browser.getString("Username");
            Password = Browser.getString("Password");
        }

        TextView greetings = (TextView) findViewById(R.id.greeting_text);
        greetings.setText("HI, " + String.valueOf(Username));
        FireBaseMessaging.getUser(String.valueOf(Username));
        DB.getUser(String.valueOf(Username));

        DB dataBaseHelper = new DB(getApplicationContext());
        dataBaseHelper.openDatabase();
        c = dataBaseHelper.getUser();
        if (c.getCount() == 0) {
            dataBaseHelper.insertUser(Username.toLowerCase());
        } else {
            if (c.moveToFirst()) {
                String user = c.getString(1);
                if (!user.contains(Username.toLowerCase())) {
                    dataBaseHelper.insertUser(Username.toLowerCase());
                }
            }
        }
        c.close();
        dataBaseHelper.close();

        overlay = (RelativeLayout) findViewById(R.id.overlay);
        overlay.setVisibility(View.VISIBLE);

        //DEFINE SWITCH BUTTON
        notif_status = (TextView) findViewById(R.id.notif_status);

        notification_switch = (Switch) findViewById(R.id.notif_switch);
        SharedPreferences settings = getSharedPreferences(NOTIF_PERMISSION, MODE_PRIVATE);
        NotifPermission = settings.getString("PERMISSON", "false");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            WebView.setWebContentsDebuggingEnabled(true);
            if (Objects.equals(NotifPermission, "false")) {
                notification_switch.setChecked(false);
                notif_status.setText("OFF");
                notif_status.setTextColor(Color.parseColor("#FFDD0505"));
            } else {
                notification_switch.setChecked(true);
                notif_status.setText("ON");
                notif_status.setTextColor(Color.parseColor("#FF2CD71C"));
            }
        } else {
            if (NotifPermission.equals("false")) {
                notification_switch.setChecked(false);
                notif_status.setText("OFF");
                notif_status.setTextColor(Color.parseColor("#FFDD0505"));
            } else {
                notification_switch.setChecked(true);
                notif_status.setText("ON");
                notif_status.setTextColor(Color.parseColor("#FF2CD71C"));
            }
        }


        notification_switch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NotifTurn();
            }
        });
        //DEFINE SWITCH BUTTON

        //START NEW SERVICE
        MessageReceiver = new MessageReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(FireBaseMessaging.MESSAGING_ACTION);
        registerReceiver(MessageReceiver, intentFilter);
        Intent intent = new Intent(BrowseActivity.this, FireBaseMessaging.class);
        startService(intent);
        bindService(intent, mServerConn, Context.BIND_AUTO_CREATE);
        //START NEW SERVICE


        //DEFINE BROWSER
        mWebView = (ObservableWebView) findViewById(R.id.webView);

        CookieManager.getInstance().removeSessionCookie();
        CookieManager.getInstance().removeAllCookie();
        mWebView.clearView();
        mWebView.clearCache(true);

        final WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mWebView.getSettings().setAllowContentAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        }
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setSupportMultipleWindows(false);
        webSettings.setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setUserAgentString(getSharedPreferences("external_data", 0).getString("user_agent", ""));
        CookieManager.getInstance().setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView, true);
        }
        CookieManager.allowFileSchemeCookies();
//        //DEFINE BROWSER
//
//        //DEFINE MESSAGE BOX
        messagebox = (RelativeLayout) findViewById(R.id.message_box);

        // ENable login w/ fingerprint
        settingbox = (RelativeLayout) findViewById(R.id.setting_box);
        onOffSwitch = (Switch) findViewById(R.id.on_off_switch);
        mWebView.setClickable(false);
//        //DEFINE MESSAGE BOX
//
        loadingover = (RelativeLayout) findViewById(R.id.loading);

//
//        //DEFINE NOTIFICATION BUTTON
        notif_button = (ImageView) findViewById(R.id.notif_button);
        notif_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getNotifMenu();
            }
        });

        setting_button = (ImageView) findViewById(R.id.setting_button);
        setting_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getSettingMenu();
            }
        });
//
        ChangeNotif();
        myWebChromeClient mWebChromeClient = new myWebChromeClient();
        mWebView.setWebChromeClient(mWebChromeClient);
        fileChooserLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (filePathCallback != null) {
                Uri[] results = null;
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    results = new Uri[]{result.getData().getData()};
                }
                filePathCallback.onReceiveValue(results);
                filePathCallback = null;
            }

            if (filePathCallbackLegacy != null) {
                Uri resultUri = null;
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    resultUri = result.getData().getData();
                }
                filePathCallbackLegacy.onReceiveValue(resultUri);
                filePathCallbackLegacy = null;
            }
        });
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        mWebView.setDownloadListener(new DownloadListener() {

            public void onDownloadStart(final String url, String userAgent,
                                        final String contentDisposition, final String mimetype,
                                        long contentLength) {
                verifyStoragePermissions(BrowseActivity.this);

                registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

                final String filename = URLUtil.guessFileName(url, contentDisposition, mimetype);
                Snackbar snackbar = Snackbar
                        .make(mWebView, getString(R.string.toast_download_1) + " " + filename, Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.toast_yes), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                                request.addRequestHeader("Cookie", CookieManager.getInstance().getCookie(url));
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
                                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                                dm.enqueue(request);

                                Snackbar.make(mWebView, getString(R.string.toast_download) + " " + filename, Snackbar.LENGTH_SHORT).show();
                            }
                        });
                snackbar.show();
            }
        });
        mWebView.loadUrl(Constant.URL_INDEX);
//        mWebView.loadUrl("https://lobby.menangtoto.pw/encryption/IHNn4sWutHcE4sDEa36m2FmqGkgzJBFIkGHLP4sDEa36m2B4sDEa36m2By5GWRVrHdsXxzUIaZ6rj634sDEa36m2FmN6e2ZZzdC4ia4Phbv42BAriHMacb4sDEa36m2BsZw4sDEa36m3D4sDEa36m3D");
        String sites = mWebView.getUrl();
        final String statusx = Uri.parse(Constant.URL_INDEX).getQueryParameter("id");
        final String[] refsynth = {null};
        String urlinpost = urlinpos.replace("{\"", "");
        String urlinposti = urlinpost.replace("\"}", "");
        final String[] satuan = urlinposti.split(",");

        String urldm = ulrdmn.replace("{\"", "");
        String urldm2 = urldm.replace("\"}", "");
        satuandmn = urldm2.split(",");

        Log.d("CONFIG", Arrays.toString(satuandmn));
        Log.d("CONFIG", Arrays.toString(satuan));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mWebView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (mWebView.getScrollY() > 0) {
                        //                          swipe.setEnabled(false);
                    } else {
                        //                         swipe.setEnabled(true);
                    }
                }
            });
        } else {
            mWebView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    if (mWebView.getScrollY() > 0) {
                        //                      swipe.setEnabled(false);
                    } else {
                        //                      swipe.setEnabled(true);
                    }
                }
            });
        }

        final PackageInfo finalPackageInfo = packageInfo;
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String potknk = Uri.parse(url).getLastPathSegment();
                List<String> pathUrl = Uri.parse(url).getPathSegments();
//                Log.d("Test Debug", "url : " + url);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webSettings.setSupportMultipleWindows(Objects.equals(potknk, "deposit"));
                    if (Objects.equals(potknk, "ajkdfnbakjbcjkabckj.php")) {
                        refsynth[0] = Uri.parse(url).getQueryParameter("ref");
                    }
                    if (Objects.equals(potknk, "peraturan.php")) {
                        mWebView.getSettings().setUseWideViewPort(true);
                        mWebView.getSettings().setLoadWithOverviewMode(true);
                    } else if (Objects.equals(potknk, "no_active.php")) {
                        mWebView.getSettings().setUseWideViewPort(true);
                        mWebView.getSettings().setLoadWithOverviewMode(true);
                    } else {
                        mWebView.getSettings().setUseWideViewPort(false);
                        mWebView.getSettings().setLoadWithOverviewMode(false);
                    }
                } else {
                    if (potknk != null) {
                        if (potknk.equals("ajkdfnbakjbcjkabckj.php")) {
                            refsynth[0] = Uri.parse(url).getQueryParameter("ref");
                        } else if (potknk.equals("peraturan.php")) {
                            mWebView.getSettings().setUseWideViewPort(true);
                            mWebView.getSettings().setLoadWithOverviewMode(true);
                        } else if (potknk.equals("no_active.php")) {
                            mWebView.getSettings().setUseWideViewPort(true);
                            mWebView.getSettings().setLoadWithOverviewMode(true);
                        } else {
                            mWebView.getSettings().setUseWideViewPort(false);
                            mWebView.getSettings().setLoadWithOverviewMode(false);
                        }
                    }
                }

                if (Arrays.asList(satuan).contains(Uri.parse(url).getHost())) {
                    String include = statusx + "," + refsynth[0];
                    new ReportWeb().execute(include, "23", "23");
                    mWebView.loadUrl(Constant.URL_INDEX);
                    return false;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (!Objects.equals(Uri.parse(url).getHost(), Constant.URL_HOST)) {
                        if (Arrays.asList(satuandmn).contains(Uri.parse(url).getHost())) {
                            CURRENT_HOST = Uri.parse(mWebView.getUrl()).getHost();
                            mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                            return false;
                        } else if ("www.ios88app.com".equals(Uri.parse(url).getHost())
                                || "mplay88.club".equals(Uri.parse(url).getHost())
                                || "www.gameiosapk.com".equals(Uri.parse(url).getHost())) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(url));
                            startActivity(intent);
                            return true;
                            // Download APK Validation
                        } else if ("secure.livechatinc.com".equals(Uri.parse(url).getHost())) {
                            SharedPreferences pref = getSharedPreferences("external_data", 0);

                            String liveChatLicense = pref.getString("live_chat_license", "");
                            Intent chat = new Intent(BrowseActivity.this, ChatWindowActivity.class);

                            chat.putExtra(ChatWindowActivity.KEY_GROUP_ID, "0");
                            chat.putExtra(ChatWindowActivity.KEY_LICENCE_NUMBER, liveChatLicense);
                            chat.putExtra(ChatWindowActivity.KEY_VISITOR_NAME, Username);
                            chat.putExtra("DewaGG", "Android App");
                            startActivity(chat);

                            return true;
                            // Livechat Libs
                        } else if ("api.whatsapp.com".equals(Uri.parse(url).getHost())) {
                            String whatsappPackage = "com.whatsapp";
                            callMessagerApps(whatsappPackage, url, "Whatsapp");
                            return true;
                            // Whatsapp API
                        } else if ("line.me".equals(Uri.parse(url).getHost())) {
                            String linePackage = "jp.naver.line.android";
                            callMessagerApps(linePackage, url, "LINE");
                            return true;
                            // Line API
                        } else if ("t.me".equals(Uri.parse(url).getHost())) {
                            String telegramPackage = "org.telegram.messenger";
                            callMessagerApps(telegramPackage, url, "Telegram");
                            return true;
                            // Telegram API
                        }
                        if (!Arrays.asList(satuandmn).contains(Uri.parse(url).getHost())) {
                            if (url.equals("https://gameiosapk.s3-ap-southeast-1.amazonaws.com/NEW/IDNPoker-2.1.0.1.apk")
                                    || url.equals("https://mplay88.club/dewagg")) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(url));
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(BrowseActivity.this, SingleBrowser.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("url", url);
                                startActivity(intent);
                            }
                            return true;
                        }
                    }
                } else {
                    if (!Uri.parse(url).getHost().equals("103.249.162.221")) {
                        if (Arrays.asList(satuandmn).contains(Uri.parse(url).getHost())) {
                            if (Uri.parse(url).getHost().equals("secure.livechatinc.com")) {
                                /*Intent i = new Intent(BrowseActivity.this, SingleBrowser.class);
                                i.putExtra("url", url);
                                startActivity(i);*/

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("external_data", 0);
                                String liveChatLicense = pref.getString("live_chat_license", null);

                                Intent chat = new Intent(BrowseActivity.this, ChatWindowActivity.class);

                                chat.putExtra(ChatWindowActivity.KEY_GROUP_ID, "0");
                                chat.putExtra(ChatWindowActivity.KEY_LICENCE_NUMBER, liveChatLicense);
                                chat.putExtra(ChatWindowActivity.KEY_VISITOR_NAME, Username);
                                chat.putExtra("DewaGG", "Android App");
                                startActivity(chat);
                                return true;
                            }
                            CURRENT_HOST = Uri.parse(mWebView.getUrl()).getHost();
                            mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                            return false;
                        }
                        if (!Arrays.asList(satuandmn).contains(Uri.parse(url).getHost())) {
                            if (url.equals("https://gameiosapk.s3-ap-southeast-1.amazonaws.com/NEW/IDNPoker-2.1.0.1.apk")
                                    || url.equals("https://mplay88.club/dewagg")) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(url));
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(BrowseActivity.this, SingleBrowser.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("url", url);
                                startActivity(intent);
                            }
                            return true;
                        }
                    }
                }
                return false;
            }
        });


    }

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d("SERVICE", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("SERVICE", "onServiceDisconnected");
        }
    };

    private void getSettingMenu() {

        if (Settingopened == 0) {
            webView.setClickable(false);
            webView.setEnabled(false);
            settingbox.animate().translationY(0).setDuration(300);
            messagebox.animate().translationY(-3000).setDuration(300);
            Settingopened = 1;

            int idfprint = 0;
            DB dataBaseHelper = new DB(this);
            dataBaseHelper.openDatabase();
            c = dataBaseHelper.getUserFingerPrint(Username);
            if (c.getCount() != 0) {
                if (c.moveToFirst()) {
                    String status = c.getString(3);
                    idfprint = c.getInt(0);
                    if (status.contains("enable")) {
                        onOffSwitch.setChecked(true);
                    } else {
                        onOffSwitch.setChecked(false);
                    }
                }
            }
            c.close();
            dataBaseHelper.close();

            final int finalIdfprint = idfprint;
            onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.v("Switch State=", "" + isChecked);
                    DB dataBaseHelper = new DB(getApplicationContext());
                    dataBaseHelper.openDatabase();

                    String myDeviceModel = Build.MODEL;

                    if (isChecked) {
                        d = dataBaseHelper.getUserFingerPrint(Username);
                        if (d.getCount() != 0) {
                            dataBaseHelper.updateFingerprint(finalIdfprint, Username, "enable");
                        } else {
                            dataBaseHelper.insertFingerprint(Username, Password, "enable", myDeviceModel);
                        }
                        d.close();

                        SharedPreferences.Editor editor = getSharedPreferences("fp_username", MODE_PRIVATE).edit();
                        editor.putString("username", Username);
                        editor.putString("password", Password);
                        editor.commit();

                        Toast.makeText(BrowseActivity.this, "Enabled", Toast.LENGTH_SHORT).show();
                    } else {
                        c = dataBaseHelper.getUserFingerPrint(Username);
                        if (c.getCount() != 0) {
                            if (c.moveToFirst()) {
                                dataBaseHelper.updateFingerprint(c.getInt(0), Username, "disable");
                            }
                        }
                        c.close();

                        SharedPreferences.Editor editor = getSharedPreferences("fp_username", MODE_PRIVATE).edit();
                        editor.clear();
                        editor.apply();

                        Toast.makeText(BrowseActivity.this, "Disabled", Toast.LENGTH_SHORT).show();
                    }


                    dataBaseHelper.close();
                }

            });

        } else {
            webView.setClickable(true);
            webView.setEnabled(true);
            settingbox.animate().translationY(-3000).setDuration(300);
            Settingopened = 0;
        }
    }

    private void getNotifMenu() {

        if (Menuopened == 0) {
            webView.setClickable(false);
            webView.setEnabled(false);
            messagebox.animate().translationY(0).setDuration(300);
            settingbox.animate().translationY(-3000).setDuration(300);
            Menuopened = 1;

            DB dataBaseHelper = new DB(context);
            dataBaseHelper.openDatabase();

            d = dataBaseHelper.getCount();
            Log.e("Loaddb", String.valueOf(d.getCount()));
            if (d.getCount() != 0) {
                if (d.moveToFirst()) {
                    dataBaseHelper.updateCount(d.getInt(0), 0, Username);
                }

                //LOAD DB
                NoMessage.setVisibility(View.GONE);
                Loaddb();

            } else {
                NoMessage.setVisibility(View.VISIBLE);
                loadmore.setVisibility(View.GONE);
            }

            d.close();
            dataBaseHelper.close();


        } else {
            webView.setClickable(true);
            webView.setEnabled(true);
            ChangeNotif();
            notif_loading.setVisibility(View.GONE);
            messagebox.animate().translationY(-3000).setDuration(300);
            Menuopened = 0;
        }
    }

    public static void Loaddb() {

        notif_loading.setVisibility(View.VISIBLE);
        dataModels = new ArrayList<>();

        final DB dataBaseHelper = new DB(ba);
        dataBaseHelper.openDatabase();

        c = dataBaseHelper.getData();
        Log.e("Loaddb", String.valueOf(c.getCount()));

        int nId = 0;
        String target = null;
        String title = null;
        String body = null;
        String webbody = null;
        String isLink = null;
        String LinkURI = null;
        String messageID = null;
        String Notification = null;
        String messageType = null;
        String imageNotif = null;
        String nDate = null;
        String isRead = null;

        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    nId = c.getInt(0);
                    messageID = c.getString(1);
                    target = c.getString(2);
                    title = c.getString(3);
                    body = c.getString(4);
                    webbody = c.getString(5);
                    isLink = c.getString(6);
                    LinkURI = c.getString(7);
                    messageType = c.getString(8);
                    imageNotif = c.getString(9);
                    Notification = c.getString(10);
                    isRead = c.getString(11);
                    nDate = c.getString(12);
                    dataModels.add(new DataModel(nId, messageID, target, title, body, webbody, isLink, LinkURI, messageType, imageNotif, Notification, isRead, nDate));
                } while (c.moveToNext());
            }
        }

        ba.LoadMessageBox(c.getCount(), dataModels);

        if (c.getCount() == 0) {
            NoMessage.setVisibility(View.VISIBLE);
        } else {
            NoMessage.setVisibility(View.GONE);
        }

        if (c.getCount() >= 10) {
            loadmore.setVisibility(View.VISIBLE);
        } else {
            loadmore.setVisibility(View.GONE);
        }

        c.close();
        dataBaseHelper.close();


    }

    private void ChangeNotif() {
        final TextView NotifNew = (TextView) findViewById(R.id.notif_count);

        SharedPreferences sp = getSharedPreferences("notification", Activity.MODE_PRIVATE);
        mCount = sp.getInt("notifcount", 0);

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SharedPreferences sp = getSharedPreferences("notification", Activity.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putInt("notifcount", mCount);
                                editor.commit();
                                DB dataBaseHelper = new DB(getApplicationContext());
                                dataBaseHelper.openDatabase();

                                c = dataBaseHelper.getCount();
                                d = dataBaseHelper.getCountNull();
                                int count1 = 0;
                                int count2 = 0;

                                if (c.getCount() != 0) {
                                    if (c.moveToFirst()) {
                                        count1 = c.getInt(1);
                                    }
                                }


                                if (d.getCount() != 0) {
                                    if (d.moveToFirst()) {
                                        count2 = d.getInt(1);
                                    }
                                }


                                NotifNew.setText(String.valueOf(count1 + count2));
                                if (count1 + count2 == 0) {
                                    NotifNew.setVisibility(View.INVISIBLE);
                                } else {
                                    NotifNew.setVisibility(View.VISIBLE);
                                }
                                c.close();
                                d.close();
                                dataBaseHelper.close();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();


    }

    private void LoadMessageBox(int count, final ArrayList list) {

        ProgressBar notif_loading = (ProgressBar) findViewById(R.id.notif_loading);
        final TextView loadmore = (TextView) findViewById(R.id.load_more);

        adapter = new ListMessageBoxAdapter(BrowseActivity.this, list);
        adapter.notifyDataSetChanged();

        notif_list = (ListView) findViewById(R.id.notif_list);
        notif_list.setAdapter(adapter);
        notif_list.setFastScrollEnabled(true);
        notif_loading.setVisibility(View.GONE);
        notif_list.setVisibility(View.VISIBLE);
        final int[] load_number = {15};
        if (list.size() >= 15) {
            loadmore.setVisibility(View.VISIBLE);
        } else {
            loadmore.setVisibility(View.GONE);
        }
        notif_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idnotif = ((TextView) view.findViewById(R.id.notif_id)).getText().toString();
                Intent intent = new Intent(getApplicationContext(), MessageBoxSingle.class);
                intent.putExtra("MESSAGEID", idnotif);
                readmessage(Integer.parseInt(idnotif));
                intent.putExtra("USERID", Username);
                startActivity(intent);
                getNotifMenu();
            }
        });

        notif_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list.size() >= 15) {
                    if (notif_list.getLastVisiblePosition() == notif_list.getAdapter().getCount() - 1 &&
                            notif_list.getChildAt(notif_list.getChildCount() - 1).getBottom() <= notif_list.getHeight()) {
                        loadmore.setVisibility(View.VISIBLE);

                    } else {
                        loadmore.setVisibility(View.GONE);
                    }
                }
            }
        });

        loadmore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DB database = new DB(context);
                int allNotifCount = database.CountAllNotif(getApplicationContext(), Username);
                int load_numb = 8;
                if (allNotifCount < load_number[0]) {
                    load_numb = load_number[0] - allNotifCount;
                }
                MessageNotif = database.nextNotif(getApplicationContext(), Username, load_numb, load_number[0]);
                if (MessageNotif.size() != 0) {
                    load_number[0] = load_number[0] + load_numb;
                    for (int i = 0; i < MessageNotif.size(); i++) {
                        MessageArray.add(MessageNotif.get(i));
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(BrowseActivity.this, "TIDAK ADA PESAN LEBIH LANJUT", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    public void readmessage(int id) {
        DB database = new DB(context);
        database.readednotif(getApplicationContext(), Username, id);
    }


    private final BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Snackbar snackbar = Snackbar
                    .make(mWebView, getString(R.string.toast_download_2), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.toast_yes), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
                        }
                    });
            snackbar.show();
            unregisterReceiver(onComplete);
        }
    };

    @Override
    public void onBackPressed() {
        if (Menuopened == 1) {
            getNotifMenu();
        } else if (login_success == 1) {
            if (Uri.parse(mWebView.getUrl()).getLastPathSegment() == null) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(BrowseActivity.this);
                builder1.setMessage("Yakin ingin keluar?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Ya",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                CookieManager.getInstance().removeSessionCookie();
                                CookieManager.getInstance().removeAllCookie();
                                mWebView.clearView();
                                mWebView.clearCache(true);
                                isLogin = 0;
                                isLogout = 0;
                                Intent startLogin = new Intent(BrowseActivity.this, LoginActivity.class);
                                startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                finish();
                                startActivityForResult(startLogin, 0);
                                overridePendingTransition(0, 0);
                            }
                        });

                builder1.setNegativeButton(
                        "Tidak",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            } else {
                mWebView.goBack();
            }
        } else if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            CookieManager.getInstance().removeSessionCookie();
            CookieManager.getInstance().removeAllCookie();
            mWebView.clearView();
            mWebView.clearCache(true);
            isLogin = 0;
            isLogout = 0;
            Intent startLogin = new Intent(BrowseActivity.this, LoginActivity.class);
            startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            finish();
            startActivityForResult(startLogin, 0);
            overridePendingTransition(0, 0);
        }
    }

    private class myWebChromeClient extends WebChromeClient {

        Button tutuphalaman;
        // Handle file chooser for input[type="file"]
        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            BrowseActivity.this.filePathCallback = filePathCallback;
            openFileChooser();
            return true;
        }

        // For older Android versions (pre-API 21)
        @SuppressWarnings("deprecation")
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            filePathCallbackLegacy = uploadMsg;
            openFileChooser();
        }

        @SuppressWarnings("deprecation")
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            filePathCallbackLegacy = uploadMsg;
            openFileChooser();
        }

        // Open file chooser using Intent
        private void openFileChooser() {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            try {
                fileChooserLauncher.launch(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            mWebView.scrollTo(0,0);
            mWebView.removeAllViews();
            final WebView newView = new WebView(getApplicationContext());
            newView.setWebViewClient(new WebViewClient());
            // Create dynamically a new view
            newView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            mWebView.addView(newView);
            tutuphalaman = findViewById(R.id.tutuphalaman);
            tutuphalaman.setVisibility(View.VISIBLE);

            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newView);
            resultMsg.sendToTarget();
            newView.getSettings().setJavaScriptEnabled(true);
            newView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                    Log.d("Test Debug", "webchromeclient: " + url);
                    view.loadUrl(url);
                    return true;
                }
            });
            newView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onCloseWindow(WebView window) {
                    super.onCloseWindow(window);
                    if (newView != null) {
                        mWebView.removeView(newView);
                        tutuphalaman.setVisibility(View.GONE);
                    }
                }
            });
            tutuphalaman.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(BrowseActivity.this);
                    builder1.setMessage("Tutup Halaman Ini?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mWebView.removeView(newView);
                                    tutuphalaman.setVisibility(View.GONE);
                                }
                            });

                    builder1.setNegativeButton(
                            "Tidak",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });
            return true;
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(final String origin, final GeolocationPermissions.Callback callback) {

            final boolean remember = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(BrowseActivity.this);
            AlertDialog alert = builder.create();
            alert.show();
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Objects.equals(message, "loginsuccessapk")) {
                    isLogin = 1;
                    overlay.setVisibility(View.GONE);
                    isLogout = 1;
                    login_success = 1;
                    mWebView.getSettings().setJavaScriptEnabled(true);
                } else if (Objects.equals(message, "notloginapk")) {
                    if (isLogout == 1) {
                        CookieManager.getInstance().removeSessionCookie();
                        CookieManager.getInstance().removeAllCookie();
                        mWebView.clearView();
                        mWebView.clearCache(true);
                        isLogin = 0;
                        isLogout = 0;
                        Intent startLogin = new Intent(BrowseActivity.this, LoginActivity.class);
                        startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        startActivityForResult(startLogin, 0);
                        overridePendingTransition(0, 0);
                        mWebView.getSettings().setJavaScriptEnabled(true);
                    } else {
                        if (isLogin == 0) {
                            mWebView.loadUrl(loginprompt);
                        }
                    }
                } else if (Objects.equals(message, "logoutattempt")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(BrowseActivity.this);
                    builder1.setMessage("Yakin ingin keluar?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    CookieManager.getInstance().removeSessionCookie();
                                    CookieManager.getInstance().removeAllCookie();
                                    mWebView.clearView();
                                    mWebView.clearCache(true);
                                    isLogin = 0;
                                    isLogout = 0;
                                    Intent startLogin = new Intent(BrowseActivity.this, LoginActivity.class);
                                    startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    finish();
                                    SharedPreferences Running;
                                    Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = Running.edit();
                                    editor.putString("RUNNING", "false");
                                    editor.apply();
                                    startActivityForResult(startLogin, 0);
                                    overridePendingTransition(0, 0);
                                    FireBaseMessaging.getLastUser(Username);
                                }
                            });

                    builder1.setNegativeButton(
                            "Tidak",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            } else {
                if (message.equals("loginsuccessapk")) {
                    isLogin = 1;
                    overlay.setVisibility(View.GONE);
                    isLogout = 1;
                    login_success = 1;
                    mWebView.getSettings().setJavaScriptEnabled(true);
                } else if (message.equals("notloginapk")) {
                    if (isLogout == 1) {
                        CookieManager.getInstance().removeSessionCookie();
                        CookieManager.getInstance().removeAllCookie();
                        mWebView.clearView();
                        mWebView.clearCache(true);
                        isLogin = 0;
                        isLogout = 0;
                        Intent startLogin = new Intent(BrowseActivity.this, LoginActivity.class);
                        startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        startActivityForResult(startLogin, 0);
                        overridePendingTransition(0, 0);
                        mWebView.getSettings().setJavaScriptEnabled(true);
                    } else {
                        if (isLogin == 0) {
                            mWebView.loadUrl(loginprompt);
                        }
                    }
                } else if (message.equals("logoutattempt")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(BrowseActivity.this);
                    builder1.setMessage("Yakin ingin keluar?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    CookieManager.getInstance().removeSessionCookie();
                                    CookieManager.getInstance().removeAllCookie();
                                    mWebView.clearView();
                                    mWebView.clearCache(true);
                                    isLogin = 0;
                                    isLogout = 0;
                                    Intent startLogin = new Intent(BrowseActivity.this, LoginActivity.class);
                                    startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    finish();
                                    SharedPreferences Running;
                                    Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = Running.edit();
                                    editor.putString("RUNNING", "false");
                                    editor.apply();
                                    startActivityForResult(startLogin, 0);
                                    overridePendingTransition(0, 0);
                                }
                            });

                    builder1.setNegativeButton(
                            "Tidak",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }

            result.confirm();
            return true;
        }

        public void onProgressChanged(WebView view, int progress) {
            String xurl = mWebView.getUrl();
            if (progress == 100) {
                if (isLoading == 1) {
                    loadingover.setVisibility(View.GONE);
                    isLoading = 0;
                    mWebView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return false;
                        }
                    });
                }
            } else {
                if (isLoading == 0) {
                    loadingover.setVisibility(View.VISIBLE);
                    isLoading = 1;
                    mWebView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });
                }
            }
            if (mWebView.getUrl() != null) {
                Uri urlfull = Uri.parse(xurl);
                String segment = urlfull.getLastPathSegment();
                String Fragment = urlfull.getFragment();
                List<String> others = urlfull.getPathSegments();
                if (Fragment == null) {
                    Fragment = "null";
                }
                if (segment == null) {
                    segment = "null";
                }
                String host = urlfull.getHost();
                invalidateOptionsMenu();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (!Objects.equals(Uri.parse(mWebView.getUrl()).getHost(), "103.249.162.221")) {
                        if (Arrays.asList(satuandmn).contains(Uri.parse(mWebView.getUrl()).getHost())) {
                            CURRENT_HOST = Uri.parse(mWebView.getUrl()).getHost();
                        }
                    }
                } else {
                    if (mWebView.getUrl() != null) {
                        if (!Uri.parse(mWebView.getUrl()).getHost().equals("103.249.162.221")) {
                            if (Arrays.asList(satuandmn).contains(Uri.parse(mWebView.getUrl()).getHost())) {
                                CURRENT_HOST = Uri.parse(mWebView.getUrl()).getHost();
                            }
                        }
                    }
                }


                if (Uri.parse(xurl).getHost() != null) {
                    if (!Uri.parse(xurl).getHost().equals("103.249.162.221")) {
                        if (progress == 100) {
                            if (isLogin == 0) {
                                mWebView.loadUrl(jsLogin);
                                mWebView.loadUrl(jsLogout);
                            }
                        } else {
                            if (isLogin == 0) {
                                mWebView.loadUrl(jsLogout);
                            } else {
                                mWebView.loadUrl(jsLogin);
                                mWebView.loadUrl(jsLogout);
                            }
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if (Objects.equals(urlfull.getLastPathSegment(), "no_active.php")) {
                                overlay.setVisibility(View.GONE);
//                                swipe.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (urlfull.getLastPathSegment().equals("no_active.php")) {
                                overlay.setVisibility(View.GONE);
//                                swipe.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                }
            }
        }
    }

    private void NotifTurn() {
        SharedPreferences settings = getSharedPreferences(NOTIF_PERMISSION, MODE_PRIVATE);
        NotifPermission = settings.getString("PERMISSON", "true");
        SharedPreferences.Editor editor = settings.edit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Objects.equals(NotifPermission, "false")) {
                editor.putString("PERMISSON", "true");
                notification_switch.setChecked(true);
                notif_status.setText("ON");
                notif_status.setTextColor(Color.parseColor("#FF2CD71C"));
                FireBaseMessaging.notifStatus("ON");
            } else {
                editor.putString("PERMISSON", "false");
                notification_switch.setChecked(false);
                notif_status.setText("OFF");
                notif_status.setTextColor(Color.parseColor("#FFDD0505"));
                FireBaseMessaging.notifStatus("OFF");
            }
        } else {
            if (NotifPermission.equals("false")) {
                editor.putString("PERMISSON", "true");
                notification_switch.setChecked(true);
                notif_status.setText("ON");
                notif_status.setTextColor(Color.parseColor("#FF2CD71C"));
                FireBaseMessaging.notifStatus("ON");
            } else {
                editor.putString("PERMISSON", "false");
                notification_switch.setChecked(false);
                notif_status.setText("OFF");
                notif_status.setTextColor(Color.parseColor("#FFDD0505"));
                FireBaseMessaging.notifStatus("OFF");
            }
        }

        editor.apply();
    }

    private class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            if (Menuopened == 0) {
                ChangeNotif();
            } else {
                Loaddb();
            }
        }
    }

    @Override
    protected void onStop() {
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "false");
        editor.apply();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private class ReportWeb extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... mainjson) {
            trigger(mainjson[0]);
            return "success";
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
            super.onPostExecute(result);
        }

        private void trigger(String mainjson) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                final String[] satuan = mainjson.split(",");
                String id = satuan[0];
                String ref = satuan[1];
                json.put("id", id);
                json.put("ref", ref);
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(json));
                Request request = new Request.Builder()
                        .url(Constant.URL_REPORT)
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
            } catch (IOException ignored) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static class GetConfL extends AsyncTask<String, String, String> {

        public Exception exception;

        @SuppressLint("NewApi")
        public String doInBackground(String... urls) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.URL_LOGIN_CONF)
                    .get()
                    .addHeader("content-type", "text/xml")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
            super.onPostExecute(result);
        }
    }

    private boolean appInstalledOrNot(String s) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(s, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    // Func API
    private void callMessagerApps(String appsPackage, String url, String appsName) {
        try {
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo(appsPackage, PackageManager.GET_ACTIVITIES);
            Intent appsIntent = new Intent(Intent.ACTION_VIEW);
            appsIntent.setData(Uri.parse(url));
            startActivity(appsIntent);

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(BrowseActivity.this, "Download the " + appsName + " app first!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appsPackage)));
        }
    }
}
