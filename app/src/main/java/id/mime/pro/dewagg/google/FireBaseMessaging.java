package id.mime.pro.dewagg.google;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import id.mime.pro.dewagg.Constant;
import id.mime.pro.dewagg.MainActivity;
import id.mime.pro.dewagg.R;
import id.mime.pro.dewagg.mime.CursorUtils;
import id.mime.pro.dewagg.mime.FunctionJava;
import id.mime.pro.dewagg.storage.DB;
import me.leolin.shortcutbadger.ShortcutBadger;

import static id.mime.pro.dewagg.MainActivity.IS_RUNNING;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class FireBaseMessaging extends FirebaseMessagingService {

    private static final String TAG = FireBaseMessaging.class.getSimpleName();
    public final static String MESSAGING_ACTION = "MESSAGING_ACTION";

    Context context;
    private Map<String, String> data;
    static String user, lastuser, status;
    Cursor c;
    int count;
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
        createNotificationChannel();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        data = remoteMessage.getData();
        Log.i(TAG, "onMessageReceived: " + data);

        context = this;
        count = 0;

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
//        Log.d("FIREBASE", "From: " + remoteMessage.getFrom());
//        Log.d("FIREBASE", String.valueOf(remoteMessage.getData()));
        // Check if message contains a data payload.
        if (data.size() > 0) {
            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            final PendingIntent pendingIntent = PendingIntent.getActivity(this,
                    0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            String target = data.get("target").toLowerCase();
            String imageNotif = data.get("imgntf");
            String Notification = data.get("NotifMessage");

            if (status == null) {
                status = "OFF";
            }

            if ("".equals(target)) {
                ArrayList<String> phoneuser = new ArrayList<String>();
                DB dataBaseHelperrr = new DB(context);
                dataBaseHelperrr.openDatabase();

                c = dataBaseHelperrr.getAllUser();
                if (c.getCount() > 0) {
                    for (Cursor cur : CursorUtils.iterate(c)) {
                        String user = cur.getString(1);
                        phoneuser.add(user);
                    }
                }
                Log.e("userr", String.valueOf(phoneuser));
                c.close();
                dataBaseHelperrr.close();

                for (String username : phoneuser) {
                    saveDataToDb(data, username, imageNotif, Notification);
                }
            } else {
                if (status.equals("OFF")) {
//                    NotificationCompat.Builder notificationBuilder = buildNotif(data, pendingIntent);
//                    notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                    notificationManager.notify(notificationId, notificationBuilder.build());
                    newBuildNotif(data, pendingIntent);
                    FunctionJava.playNotificationSound(context, "asnotif");
                    FunctionJava.turnOnScreen(context);
                }
                saveDataToDb(data, target, imageNotif, Notification);
            }
        }

        Intent broadcast = new Intent("NewNotif");
        broadcast.putExtra("isNew", true);
        broadcaster.sendBroadcast(broadcast);
    }

    // Also if you intend on generating your own notifications as a result of a received FCM
    // message, here is where that should be initiated. See sendNotification method below.
    private void saveDataToDb(Map<String, String> data, String target, String imageNotif, String Notification) {
        String title = data.get("title");
        String content = data.get("body");
        String webbody;
        if (data.containsKey("webbody")) {
            webbody = data.get("webbody");
        } else {
            webbody = "null";
        }
        Log.e("web", webbody);
        String isLink = data.get("isLink");
        String LinkURI = data.get("LinkURI");
        String messageID = data.get("messageID");
        String messageType = data.get("type").toLowerCase();


        DB dataBaseHelper = new DB(context);
        dataBaseHelper.openDatabase();
        dataBaseHelper.insertData(messageID, target, title, content, webbody,
                isLink, LinkURI, messageType, imageNotif, Notification, "false",
                setCurrentDate());

        c = dataBaseHelper.getCount();
        if (c.getCount() == 0) {
            dataBaseHelper.insertCount(1, target);
        } else {
            if (c.moveToFirst()) {
                int count1 = c.getInt(1);
                count = ++count1;
                Log.e("count", String.valueOf(count));
                dataBaseHelper.updateCount(c.getInt(0), count, target);
                ShortcutBadger.applyCount(getApplicationContext(), count);
            }
        }

        c.close();
        dataBaseHelper.close();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "DewaVegas";
            String description = "Notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getApplicationContext().getPackageName(), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private NotificationCompat.Builder buildNotif(Map<String, String> data, PendingIntent pendingIntent) {
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this, getApplicationContext().getPackageName());
        long[] vibrate = {0, 100, 200, 300};
        Bitmap bitmap = getBitmapfromUrl(data.get("imgntf"));

        notifBuilder.setSmallIcon(R.mipmap.ic_stat_ic_notification);
        notifBuilder.setLargeIcon(bitmap);
        notifBuilder.setContentTitle(data.get("title"));
        notifBuilder.setContentText(data.get("NotifMessage"));
        notifBuilder.setAutoCancel(true);
        notifBuilder.setColor(0xFF264361);
        notifBuilder.setVibrate(vibrate);
        notifBuilder.setNumber(1);
        notifBuilder.setSound(Uri.parse("android.resource://id.mime.pro.dewagg/"+R.raw.asnotif));
        notifBuilder.setContentIntent(pendingIntent);

        if (bitmap != null) {
            notifBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .setSummaryText(data.get("NotifMessage"))
                    .bigPicture(bitmap));
        }

        return notifBuilder;
    }

    private String setCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = Calendar.getInstance().getTime();
        return dateFormat.format(date);
    }

    private String checkRUNING() {
        SharedPreferences settings = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        return settings.getString("RUNNING", "0");
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void notifStatus(String stat) {
        status = stat;
    }

    public static void getUser(String userid) {
        user = userid.toLowerCase();
    }

    public static void getLastUser(String userid) {
        lastuser = userid.toLowerCase();
    }

    private void newBuildNotif(Map<String, String> data, PendingIntent pendingIntent) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        long[] vibrate = {0, 100, 200, 300};
        int notificationId = new Random().nextInt(60000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(Constant.GAME_NAME, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            notificationChannel.setDescription(data.get("title"));
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(vibrate);
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, Constant.GAME_NAME);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(android.app.Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_stat_ic_notification)
                .setContentTitle(data.get("title"))
                .setContentText(data.get("NotifMessage"))
                .setColor(0xFF264361)
                .setVibrate(vibrate)
                .setNumber(1)
                .setSound(Uri.parse("android.resource://id.mime.pro.dewagg/"+R.raw.asnotif))
                .setContentIntent(pendingIntent);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

}

