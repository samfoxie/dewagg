package id.mime.pro.dewagg.google;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class autostart extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intents = new Intent(context,FireBaseMessaging.class);
        context.startService(intents);
        Log.i("Autostart", "started");
    }
}
