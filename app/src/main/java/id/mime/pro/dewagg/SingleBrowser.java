package id.mime.pro.dewagg;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class SingleBrowser extends AppCompatActivity {

    private String configL;
    private int isLoading = 0;

    private WebView mWebView;

    private static final int FULL_SCREEN_SETTING = View.SYSTEM_UI_FLAG_FULLSCREEN |
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
            View.SYSTEM_UI_FLAG_IMMERSIVE;

    private static final String TAG = SingleBrowser.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_browser);

        String url = getIntent().getStringExtra("url");

        Log.d(TAG, "single browser " + url);

        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.clearCache(true);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setSupportMultipleWindows(false);
        webSettings.setAllowFileAccessFromFileURLs(true); //Maybe you don't need this rule
        webSettings.setUserAgentString(getSharedPreferences("external_data", 0).getString("user_agent", ""));
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        // Enable responsive layout
        webSettings.setUseWideViewPort(true);
        // Zoom out if the content width is greater than the width of the viewport
        webSettings.setLoadWithOverviewMode(true);
        // Configure the client to use when opening URLs
        WebChromeClientCustom mWebChromeClient = new WebChromeClientCustom();
        mWebView.setWebChromeClient(mWebChromeClient);
        mWebView.setWebViewClient(new WebViewClient());

        mWebView.loadUrl(url);

        configL = null;
        try {
            configL = new BrowseActivity.GetConfL().execute().get();

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        int try_connect = 0;
        while (configL == null) {
            try_connect = try_connect + 1;
            if (try_connect > 5) {
                //NO CONNECTION
                Intent error = new Intent(this, ErrorActivity.class);
                error.putExtra("ERROR", "CONNECTION");
                startActivity(error);
                //NO CONNECTION
            } else {
                try {
                    configL = new BrowseActivity.GetConfL().execute().get();
                } catch (InterruptedException | ExecutionException ignored) {

                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (mWebView.getUrl().contains("id2.sg8bet.com")) {
            finish();
        }
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }

    private class WebChromeClientCustom extends WebChromeClient {
        private View mCustomView;
        private CustomViewCallback mCustomViewCallback;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        @Override
        public void onProgressChanged(WebView view, int progress) {
            String xurl = mWebView.getUrl();

            if (progress == 100) {
                if (isLoading == 1) {
                    isLoading = 0;
                    mWebView.setOnTouchListener((v, event) -> false);
                }
            } else {
                if (isLoading == 0) {
                    isLoading = 1;
                    mWebView.setOnTouchListener((v, event) -> true);
                }
            }
            if (mWebView.getUrl() != null) {
                Uri urlfull = Uri.parse(xurl);
                String segment = urlfull.getLastPathSegment();
                String Fragment = urlfull.getFragment();
                List<String> others = urlfull.getPathSegments();
                if (Fragment == null) {
                    Fragment = "null";
                }
                if (segment == null) {
                    segment = "null";
                }
                invalidateOptionsMenu();
            }
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            String lps = Uri.parse(url).getLastPathSegment();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Objects.equals(lps, "home")) {
                    if (message.equals("loginsuccessapk")) {
                        Toast.makeText(SingleBrowser.this, "Sukses Register, Silahkan Login Kembali", Toast.LENGTH_LONG).show();
                        logout();
                    }
                }
            }
            result.confirm();
            return true;
        }

        /* Setting For Force Fullscreen */
        public void onHideCustomView() {
            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        }

        public void updateControls() {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) this.mCustomView.getLayoutParams();
            params.bottomMargin = 0;
            params.topMargin = 0;
            params.leftMargin = 0;
            params.rightMargin = 0;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            this.mCustomView.setLayoutParams(params);
            SingleBrowser.this.getWindow().getDecorView().setSystemUiVisibility(FULL_SCREEN_SETTING);
        }

        public void onShowCustomView(View paramView, CustomViewCallback paramCustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout) getWindow()
                    .getDecorView())
                    .addView(this.mCustomView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            SingleBrowser.this.getWindow().getDecorView().setSystemUiVisibility(FULL_SCREEN_SETTING);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
            this.mCustomView.setOnSystemUiVisibilityChangeListener(visibility -> WebChromeClientCustom.this.updateControls());
        }
        /* Setting Force Fullscreen */
    }

    protected void logout() {
        CookieManager.getInstance().removeSessionCookie();
        CookieManager.getInstance().removeAllCookie();
        mWebView.clearView();
        mWebView.clearCache(true);
        Intent startLogin = new Intent(SingleBrowser.this, LoginActivity.class);
        startLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        startActivityForResult(startLogin, 0);
        overridePendingTransition(0, 0);
    }
}