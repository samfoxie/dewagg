package id.mime.pro.dewagg.storage;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by userIDN on 3/27/2018.
 */

public class DataModel {

    int notifId;
    String target;
    String title;
    String body;
    String webbody;
    String isLink;
    String LinkURI;
    String messageID;
    String NotifMessage;
    String type;
    String imgNtf;
    String nDate;
    String isRead;

    public DataModel(int notifId, String messageID, String target, String title, String body, String webbody, String isLink, String LinkURI, String type, String imgNtf, String NotifMessage, String isRead, String nDate) {
        this.notifId=notifId;
        this.messageID=messageID;
        this.target=target;
        this.title=title;
        this.body = body;
        this.webbody = webbody;
        this.isRead = isRead;
        this.isLink = isLink;
        this.LinkURI = LinkURI;
        this.type = type;
        this.imgNtf = imgNtf;
        this.NotifMessage = NotifMessage;
        this.isRead = isRead;
        this.nDate = nDate;

    }

    /*Comparator for sorting the list by Student Name*/
    public static Comparator<DataModel> sortByDate = new Comparator<DataModel>() {

        public int compare(DataModel p1, DataModel p2) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date o1PublishDate = null;
            Date o2PublishDate = null;
            try {
                o1PublishDate = formatter.parse(p1.getNotifDate());
                o2PublishDate = formatter.parse(p2.getNotifDate());
            } catch (ParseException pe) {
                pe.printStackTrace();
            }

            return o2PublishDate.compareTo(o1PublishDate);

        }};

    public String getIsRead(){
        return isRead;
    }

    public void setIsRead(String isRead){
        this.isRead = isRead;
    }

    public String getMessageID(){
        return messageID;
    }

    public String getNotifDate(){
        return nDate;
    }

    public  int getNotifId()
    {
        return notifId;
    }

    public String getNotifTitle() {
        return title;
    }

    public String getNotifMessage() {
        return NotifMessage;
    }

    public String getNotifImageurl() {
        return imgNtf;
    }

    public String getTarget() {
        return target;
    }
    public String getBody() {
        return body;
    }
    public String getWebbody() {
        return webbody;
    }
    public String getIsLink() {
        return isLink;
    }
    public String getLinkURI() {
        return LinkURI;
    }
    public String getType() {
        return type;
    }

}
