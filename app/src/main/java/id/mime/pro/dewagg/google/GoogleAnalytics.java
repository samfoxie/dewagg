package id.mime.pro.dewagg.google;

import android.app.Application;

import com.google.android.gms.analytics.Tracker;

/**
 * Created by pc on 4/26/2017.
 */

public class GoogleAnalytics extends Application {
    private static com.google.android.gms.analytics.GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        sAnalytics = com.google.android.gms.analytics.GoogleAnalytics.getInstance(this);
    }


    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker("UA-98097519-1");
        }

        return sTracker;

    }
}