package id.mime.pro.dewagg;

public class Constant {
    public static final String GAME_CODE = "dwgg";
    public static final String GAME_NAME = "dewagg";
    public static final String GAME_ID = "50";

    public static final String URL_IP = "https://api.cloud.uangloka.com/auth/ipos_lnk.php";
    public static final String URL_LOGIN = "https://api.cloud.uangloka.com/external/mime-loginidngoal.php";
    public static final String URL_INDEX = "https://api.cloud.uangloka.com/indexv2.php?id=" + GAME_ID;
    public static final String URL_DL_APK = "http://api214.cloud.mime.services/download_dewagg.php";
    public static final String URL_DOMAIN = "https://api.cloud.uangloka.com/auth/getalldomain.php?id=" + GAME_ID;
    public static final String URL_REPORT = "https://api.cloud.uangloka.com/report_web.php";
    public static final String URL_VERSION = "http://api214.cloud.mime.services/version_sport.php?game=" + GAME_NAME;
    public static final String URL_REG_TOKEN = "https://api.cloud.uangloka.com/external/mime_device.php?game=" + GAME_NAME;
    public static final String URL_LOGIN_CONF = "https://api.cloud.uangloka.com/auth/login_conf.php?id=" + GAME_ID;
    public static final String URL_PERMISSION = "https://api.cloud.uangloka.com/auth/ask_permission.php?id=" + GAME_ID;
    public static final String URL_ENDPOINT = "https://api.cloud.uangloka.com/auth/endpoint_url.php?id=" + GAME_ID;
    public static final String DIRECT_DL_URL = "https://api.cloud.uangloka.com/auth/geturldownload.php?gamename=" + GAME_NAME;

    public static final String URL_HOST = "api.cloud.uangloka.com";
}
