package id.mime.pro.dewagg.storage;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DB {
    public static final int DATABASE_VERSION = Context.MODE_PRIVATE;
    public static final String DATABASE_NAME = "DB-1.db";
    private SQLiteDatabase db;
    private SQLiteOpenHelper _openHelper;
    SQLiteDatabase dbase;
    static String userid;

    public DB(Context context) {
        _openHelper = new SimpleSQLiteOpenHelper(context);
    }

    class SimpleSQLiteOpenHelper extends SQLiteOpenHelper {
        SimpleSQLiteOpenHelper(Context context) {
            super(context, "notification.db", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table notification (_id integer primary key autoincrement, messageId text, target text, title text, body text, webbody text, isLink text, LinkURI text, type text, imgntf text, NotifMessage text, isRead text, date text)");
            db.execSQL("create table count (_id integer primary key autoincrement, count integer, user text)");
            db.execSQL("create table user (_id integer primary key autoincrement, username text)");
            db.execSQL("create table fingerprint (_id integer primary key autoincrement, username text, password text, status text, device text)");
            dbase = db;
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }


    public void openDatabase() {
        this.dbase = _openHelper.getWritableDatabase();
    }

    public void close() {
        dbase.close();
    }

    public void CreateNotifDb(Context ctx, String user){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS `notif_"+ user +"` (`id` int(255), `messageID` VARCHAR(255) not null unique ,`title` VARCHAR(255),`content` longtext,`isLink` int(2),`LinkURI` longtext, `is_read` int(2),`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`));");
        db.close();
    }

    public int CountNotif(Context ctx, String user){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        Cursor mcursor = db.rawQuery("SELECT count(*) FROM `notif_" + user + "` WHERE `is_read` = '0'", null);
        mcursor.moveToFirst();
        return mcursor.getInt(0);
    }

    public int CountAllNotif(Context ctx, String user){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        Cursor mcursor = db.rawQuery("SELECT count(*) FROM `notif_" + user + "` WHERE 1", null);
        mcursor.moveToFirst();
        return mcursor.getInt(0);
    }

    public boolean isNotifEmpty(Context ctx, String user){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        Cursor mcursor = db.rawQuery("SELECT count(*) FROM `notif_" + user + "`", null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0){
            return false;
        }
        else {
            return true;
        }
    }

    public int getlastidnotif(Context ctx, String user){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);

        Cursor mcursor = db.rawQuery("SELECT count(*) FROM `notif_" + user + "`", null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0){
            Cursor result = db.rawQuery("SELECT `id` FROM `notif_" + user + "` WHERE 1 ORDER BY `id` DESC LIMIT 1;",null);
            result.moveToFirst();
            if(result == null){
                return 0;
            }
            else {
                return result.getInt(0);
            }
        }
        else {
            return 0;
        }

    }

    public void InsertNotif(Context ctx, String user, int id, String title, String content, String isLink, String LinkURI, String messageID) throws Exception {
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        db.execSQL("INSERT INTO `notif_" + user + "` (`id`,`title`, `content`, `isLink`, `LinkURI`, `is_read`, `messageID`) VALUES ('" + id + "','" + title + "','" + content + "','" + isLink + "','" + LinkURI + "','0','"+messageID+"')");
        db.close();
    }

    public List<String[]> readNotif(Context ctx, String user, int start){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        Cursor result = db.rawQuery("SELECT * FROM `notif_" + user + "` WHERE 1 ORDER BY `id` DESC LIMIT 0," + start + ";" ,null);
        result.moveToFirst();
        List<String[]> where = new ArrayList<>();
        for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
            int id = result.getInt(0);
            String title = result.getString(2);
            String content = result.getString(3);
            int isLink = result.getInt(4);
            String LinkURI = result.getString(5);
            int is_read = result.getInt(6);
            String date = result.getString(7);
            String[] hasil = {Integer.toString(id),title,content, Integer.toString(isLink),LinkURI,Integer.toString(is_read),date};
            where.add(hasil);
        }
        return where;
    }

    public List<String[]> nextNotif(Context ctx, String user, int start, int after){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        Log.d("MIME","SELECT * FROM `notif_" + user + "` WHERE 1 ORDER BY `id` DESC LIMIT "+ after +"," + start + ";");
        Cursor result = db.rawQuery("SELECT * FROM `notif_" + user + "` WHERE 1 ORDER BY `id` DESC LIMIT "+ after +"," + start + ";" ,null);
        result.moveToFirst();
        List<String[]> where = new ArrayList<>();
        for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
            int id = result.getInt(0);
            String title = result.getString(2);
            String content = result.getString(3);
            int isLink = result.getInt(4);
            String LinkURI = result.getString(5);
            int is_read = result.getInt(6);
            String date = result.getString(7);
            String[] hasil = {Integer.toString(id),title,content, Integer.toString(isLink),LinkURI,Integer.toString(is_read),date};
            where.add(hasil);
        }
        return where;
    }

    public String[] loadnotif(Context ctx, String user, int id){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        String query = "SELECT `title`,`content`,`isLink`,`LinkURI` FROM `notif_" + user + "` WHERE `id` = "+id+"";
        Cursor result = db.rawQuery(query,null);
        result.moveToFirst();
        String title = result.getString(0);
        String content = result.getString(1);
        String islink = result.getString(2);
        String Link = result.getString(3);
        String[] hasil = {title,content,islink,Link};
        db.close();
        return  hasil;
    }

    public void readednotif(Context ctx, String user, int id){
        db = ctx.openOrCreateDatabase(DATABASE_NAME,DATABASE_VERSION,null);
        String query = "UPDATE `notif_"+user+"` SET `is_read` = '1' WHERE `id` = " +id+"";
        db.execSQL(query);
    }

    public Cursor getData() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "SELECT * FROM notification WHERE target = '"+ userid +"' OR target = '' ORDER BY date DESC";
        Log.e("userrrr", userid);
        return db.rawQuery(query, null);
    }

    public Cursor getCount() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "SELECT * FROM count WHERE user = '"+ userid +"'";
        return db.rawQuery(query, null);

    }

    public Cursor getCountNull() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "SELECT * FROM count WHERE user = ''";
        return db.rawQuery(query, null);
    }

    public Cursor getUser() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "SELECT * FROM user WHERE username = '"+ userid +"'";
        return db.rawQuery(query, null);
    }

    public Cursor getAllUser() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "SELECT * FROM user";
        return db.rawQuery(query, null);
    }

    /**
     * Return values for a single row with the specified id
     * @param id The unique id for the row o fetch
     * @return All column values are stored as properties in the ContentValues object
     */
    public ContentValues get(long id) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        ContentValues row = new ContentValues();
        Cursor cur = db.rawQuery("select _id, title, message, imageurl, date from notification where _id = id", new String[] { String.valueOf(id) });
        if (cur.moveToNext()) {
            row.put("id", cur.getLong(0));
            row.put("title", cur.getString(1));
            row.put("message", cur.getInt(2));
            row.put("imageurl", cur.getInt(3));
            row.put("date", cur.getString(4));
        }
        cur.close();
        db.close();
        return row;
    }

    public Cursor getUserFingerPrint(String username) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "SELECT * FROM fingerprint WHERE username='"+ username.toLowerCase() +"'";
        return db.rawQuery(query, null);
    }

    public long insertFingerprint(String username, String password, String status, String device) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        row.put("username", username.toLowerCase());
        row.put("password", password);
        row.put("status", status);
        row.put("device", device);

        long id = db.insert("fingerprint", null, row);
        db.close();
        return id;
    }

    public long updateFingerprint(int _id, String user, String status) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        Log.e("status", status);
        row.put("status", status);

        long id = db.update("fingerprint",row,"_id="+_id+" AND username='"+user.toLowerCase()+"'",null);
        db.close();
        return id;
    }





    public long insertData(String messageId, String target, String title, String body, String webbody, String isLink, String LinkURI, String type, String imgNtf, String NotifMessage, String isRead, String date) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        row.put("messageId", messageId);
        row.put("target", target);
        row.put("title", title);
        row.put("body", body);
        row.put("webbody", webbody);
        row.put("isLink", isLink);
        row.put("LinkURI", LinkURI);
        row.put("type", type);
        row.put("imgNtf", imgNtf);
        row.put("NotifMessage", NotifMessage);
        row.put("isRead", isRead);
        row.put("date", date);

        long id = db.insert("notification", null, row);
        db.close();
        return id;
    }

    public long insertCount(Integer count, String user) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        row.put("count", count);
        row.put("user", user.toLowerCase());
        long id = db.insert("count", null, row);
        db.close();
        return id;
    }

    public long insertUser(String user) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        row.put("username", user.toLowerCase());
        long id = db.insert("user", null, row);
        Log.e("userid", String.valueOf(id));
        db.close();
        return id;
    }

    public long updateCount(long _id, long count, String user) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        row.put("_id", _id);
        row.put("count", count);
        row.put("user", user.toLowerCase());


        long id = db.update("count",row,"_id="+_id+" AND user='"+user.toLowerCase()+"'",null);
        db.close();
        return id;
    }

    public Cursor loadnotification(long id) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return null;
        }

        String query = "SELECT * FROM notification WHERE _id = '"+ id +"'";

        return db.rawQuery(query, null);
    }

    public long readnotification(long id, String isread) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }

        ContentValues row = new ContentValues();
        row.put("isRead", isread);

        long _id = db.update("notification",row,"_id = ?", new String[] { String.valueOf(id) });
        db.close();
        return _id;
    }

    public long readallnotification(String isread) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }

        ContentValues row = new ContentValues();
        row.put("isRead", isread);

        long _id = db.update("notification", row,null,null);
        db.close();
        return _id;
    }


    public void delete(long id) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return;
        }

        db.delete("notification", "_id = ?", new String[] { String.valueOf(id) });
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return;
        }

        db.delete("notification", null, null );
        db.close();
    }

    public static void getUser(String user){
        userid = user.toLowerCase();
    }
}
