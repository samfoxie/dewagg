package id.mime.pro.dewagg.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Objects;

import id.mime.pro.dewagg.R;
import id.mime.pro.dewagg.storage.DB;

import static id.mime.pro.dewagg.MainActivity.IS_RUNNING;

public class MessageBoxSingle extends AppCompatActivity {

    private ImageView BACK_BUTTON;
    private TextView MESSAGE_TITLE;
    private TextView MESSAGE_CONTENT;
    private Button OPEN_LINK;
    private String username;
    private TextView GREETINGS;

    private Tracker mTracker;
    Context context;
    private Cursor c;
    private WebView mWebView;
    private RelativeLayout relWeb;
    String webBodyText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context =this;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.message_box);

        Bundle messagebox = getIntent().getExtras();

        BACK_BUTTON = (ImageView) findViewById(R.id.back_button);
        MESSAGE_TITLE = (TextView) findViewById(R.id.message_title);
        MESSAGE_CONTENT = (TextView) findViewById(R.id.message_content);
        OPEN_LINK = (Button) findViewById(R.id.link_button);
        GREETINGS = (TextView) findViewById(R.id.greeting_text);
        relWeb = (RelativeLayout) findViewById(R.id.relWeb);
        mWebView = (WebView) findViewById(R.id.webView);

        if(messagebox != null){
            final int messageid = Integer.parseInt(messagebox.getString("MESSAGEID"));
            username = messagebox.getString("USERID");
            GREETINGS.setText("Hi, "+ username);
            Log.e("notifid", String.valueOf(messageid));
            loadMessage(messageid);
            readmessage(messageid);
            BACK_BUTTON.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        else {
            finish();
        }

    }

    private void loadMessage(int id){

        DB dataBaseHelper = new DB(this);
        dataBaseHelper.openDatabase();

        c = dataBaseHelper.loadnotification(id);

        int nId = 0;
        String target = null;
        String title =null;
        String body = null;
        String webbody = null;
        String isLink = null;
        String LinkURI = null;
        String messageID = null;
        String Notification = null;
        String messageType = null;
        String imageNotif = null;
        String nDate = null;
        String isRead = null;

        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    nId = c.getInt(0);
                    messageID = c.getString(1);
                    target = c.getString(2);
                    title = c.getString(3);
                    body = c.getString(4);
                    webbody = c.getString(5);
                    isLink = c.getString(6);
                    LinkURI = c.getString(7);
                    messageType = c.getString(8);
                    imageNotif = c.getString(9);
                    Notification = c.getString(10);
                    isRead = c.getString(11);
                    nDate = c.getString(12);
                } while (c.moveToNext());
            }
        }
        c.close();
        dataBaseHelper.close();

        MESSAGE_TITLE.setText(title);
        if(webbody.equals("null")){
            relWeb.setVisibility(View.GONE);
            MESSAGE_CONTENT.setText(body);
        }else{
            MESSAGE_CONTENT.setVisibility(View.GONE);
            relWeb.setVisibility(View.VISIBLE);


            try {
                webBodyText = URLDecoder.decode(webbody, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            mWebView.loadData(webBodyText, "text/html; charset=utf-8", "UTF-8");
            mWebView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    WebView.HitTestResult hr = ((WebView) v).getHitTestResult();
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        if (hr.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE || hr.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE) {
                            Message msg = mHandler.obtainMessage();
                            mWebView.requestFocusNodeHref(msg);
                            if (Uri.parse(msg.getData().getString("url")).toString().startsWith("http")) {
                                return true;
                            }
                        }
                    }
                    return false;
                }

            });
        }


        String is_Link = isLink;
        if(is_Link != "null") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Objects.equals(is_Link, "1")) {
                    OPEN_LINK.setVisibility(View.VISIBLE);
                    final String finalLinkURI = LinkURI;
                    OPEN_LINK.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            String urlString = finalLinkURI;
                            Intent intent = new Intent(MessageBoxSingle.this, ExternalBrowser.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("url", urlString);
                            startActivity(intent);
                        }
                    });
                } else {
                    OPEN_LINK.setVisibility(View.GONE);
                }
            } else {
                if (is_Link == "1") {
                    OPEN_LINK.setVisibility(View.VISIBLE);
                    final String finalLinkURI1 = LinkURI;
                    OPEN_LINK.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            String urlString = finalLinkURI1;
                            Intent intent = new Intent(MessageBoxSingle.this, ExternalBrowser.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("url", urlString);
                            startActivity(intent);
                        }
                    });
                } else {
                    OPEN_LINK.setVisibility(View.GONE);
                }
            }
        }else{
            OPEN_LINK.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onStop()
    {
//        unregisterReceiver(MessageReceiver);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING","false");
        editor.apply();
    }

    public void readmessage(int id){
        DB dataBaseHelper = new DB(context);
        dataBaseHelper.openDatabase();
        dataBaseHelper.readnotification(id, "true");
        dataBaseHelper.close();
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // Get link-URL.
            String url = (String) msg.getData().get("url");

            if (url.startsWith("http")){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                if(context != null) {
                    context.startActivity(intent);
                }
            }
        }
    };

}

