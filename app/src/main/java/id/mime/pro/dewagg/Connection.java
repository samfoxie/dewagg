package id.mime.pro.dewagg;

import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Connection extends AppCompatActivity{

    public String PACKAGE_NAME;

    //GET CONFIGURATION
    public String getConfiguration(String[] userdata) throws ExecutionException, InterruptedException {
        String userconfig = userdata[2] + "+" + userdata[3] + "+" + userdata[4] + "+" + userdata[5];
        String result = new ConnectConfig().execute(userconfig,"1","0").get();
//        Log.d("Test Debug", "json result: " + result);
        return String.valueOf(result);
    }

    private class ConnectConfig extends AsyncTask<String,String,String>  {

        @Override
        protected String doInBackground(String... userConfig) {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            JSONObject json = new JSONObject();

            String[] result = userConfig[0].split("\\+");

//            String encodedUser = Base64.encodeToString(result[0].getBytes(), Base64.DEFAULT);
//            String encodedPass = Base64.encodeToString(result[1].getBytes(), Base64.DEFAULT);
            String PackageName = Base64.encodeToString(result[0].getBytes(), Base64.DEFAULT);
            String encodedUsId = Base64.encodeToString(result[1].getBytes(), Base64.DEFAULT);
            String encodedPaId = Base64.encodeToString(result[2].getBytes(), Base64.DEFAULT);
            String encodedUID = Base64.encodeToString(result[3].getBytes(), Base64.DEFAULT);
            try {
                json.put("package", PackageName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                json.put("usid", encodedUsId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                json.put("paid", encodedPaId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                json.put("devid", encodedUID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                json.put("code", Constant.GAME_CODE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, String.valueOf(json));
            Request request = new Request.Builder()
                    .url(Constant.URL_LOGIN)
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("cache-control", "no-cache")
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                return "0";

            }

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
    //GET CONFIGURATION

    //REGISTER DEVICE
    public void RegisterDevice(String uid){
        new RegDevice().execute(uid,"23","23");
    }

    private class RegDevice extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... mainjson) {
            trigger(mainjson[0]);
            return "success";
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
//                hasil[0] = result;
            super.onPostExecute(result);
        }

        private void trigger(String mainjson) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                String uid = mainjson;
                json.put("deviceid", uid);
                json.put("code",Constant.GAME_CODE);
                json.put("action","register");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(json));
                Request request = new Request.Builder()
                        .url(Constant.URL_REG_TOKEN  )
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
            } catch (IOException ignored) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //REGISTER DEVICE


    //REGISTER DEVICETYPE
    public void RegisterDeviceType(String deviceType){
        new RegDevType().execute(deviceType,"23","23");
    }

    private class RegDevType extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... mainjson) {
            trigger(mainjson[0]);
            return "success";
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
//                hasil[0] = result;
            super.onPostExecute(result);
        }

        private void trigger(String mainjson) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                final String[] satuan = mainjson.split("&");
                String devtype = satuan[0];
                String uid = satuan[1];
                json.put("deviceid", uid);
                json.put("code",Constant.GAME_CODE);
                json.put("devicetype", devtype);
                json.put("action","devicetype");
                OkHttpClient client = new OkHttpClient();

                RequestBody body = RequestBody.create(JSON, String.valueOf(json));
                Request request = new Request.Builder()
                        .url(Constant.URL_REG_TOKEN  )
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
            } catch (IOException ignored) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //REGISTER DEVICETYPE

    //REGISTER LOCATION
    public void RegisterDeviceLocation(String Location){
        new RegDevLocation().execute(Location,"23","23");
    }

    private class RegDevLocation extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... mainjson) {
            trigger(mainjson[0]);
            return "success";
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
//                hasil[0] = result;
            super.onPostExecute(result);
        }

        private void trigger(String mainjson) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                final String[] satuan = mainjson.split("&");
                String location = satuan[0];
                String uid = satuan[1];
                json.put("deviceid", uid);
                json.put("code",Constant.GAME_CODE);
                json.put("location", location);
                json.put("action","location");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(json));

                Request request = new Request.Builder()
                        .url(Constant.URL_REG_TOKEN  )
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
            } catch (IOException ignored) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //REGISTER LOCATION

    //REGISTER LOCATION GAPI
    public void RegisterAPILocation(String Location){
        new RegGAPILocation().execute(Location,"23","23");
    }

    private class RegGAPILocation extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... mainjson) {
            trigger(mainjson[0]);
            return "success";
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
//                hasil[0] = result;
            super.onPostExecute(result);
        }

        private void trigger(String mainjson) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                final String[] satuan = mainjson.split("&");
                String location = satuan[0];
                String uid = satuan[1];
                json.put("deviceid", uid);
                json.put("code",Constant.GAME_CODE);
                json.put("location", location);
                json.put("action","gapi");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(json));

                Request request = new Request.Builder()
                        .url(Constant.URL_REG_TOKEN  )
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
            } catch (IOException ignored) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //REGISTER LOCATION GAPI
}
