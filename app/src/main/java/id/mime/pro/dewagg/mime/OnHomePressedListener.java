package id.mime.pro.dewagg.mime;

public interface OnHomePressedListener {
    public void onHomePressed();

    public void onHomeLongPressed();
}
