package id.mime.pro.dewagg.fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import id.mime.pro.dewagg.BrowseActivity;
import id.mime.pro.dewagg.R;
import id.mime.pro.dewagg.storage.DB;
import id.mime.pro.dewagg.storage.DataModel;

public class ListMessageBoxAdapter extends BaseAdapter {
    ArrayList<DataModel> itemList;

    public Activity context;
    public LayoutInflater inflater;

    public ListMessageBoxAdapter(Activity context, ArrayList<DataModel> itemList) {
        super();

        this.context = context;
        this.itemList = itemList;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList.get(position);
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static class ViewHolder {
        RelativeLayout Box;
        TextView txtViewTitle;
        TextView txtViewDescription;
        TextView duration;
        TextView notif_id;
        ImageView imageNotif, delete;
        Context mContext;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.message_box_layout, null);
            holder.notif_id = (TextView) convertView.findViewById(R.id.notif_id);
            holder.Box = (RelativeLayout) convertView.findViewById(R.id.notifBox);
            holder.txtViewTitle = (TextView) convertView.findViewById(R.id.txtViewTitle);
            holder.txtViewDescription = (TextView) convertView.findViewById(R.id.txtViewDescription);
            holder.duration = (TextView) convertView.findViewById(R.id.duration);
            holder.imageNotif = (ImageView) convertView.findViewById(R.id.notic_pic);
            holder.delete = (ImageView) convertView.findViewById(R.id.delete);
            holder.mContext = convertView.getContext();

            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        final DataModel dataModel = itemList.get(position);

        Log.e("aaaaa", dataModel.getNotifTitle());
        holder.notif_id.setText(String.valueOf(dataModel.getNotifId()));
        holder.txtViewTitle.setText(dataModel.getNotifTitle());
        holder.txtViewDescription.setText(dataModel.getNotifMessage());
        if (dataModel.getIsRead().equals("false")) {
            holder.Box.setBackgroundColor(Color.parseColor("#FFA9D7FB"));
            //BELOM DI READ
        } else {
            holder.Box.setBackgroundColor(Color.parseColor("#ffffff"));
            //SUDAH DI READ
        }

        String dateStart, dateStop;
        dateStop = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
        dateStart = dataModel.getNotifDate();

        SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = d2.getTime() - d1.getTime();
        long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        long diffHours = TimeUnit.MILLISECONDS.toHours(diff);
        long diffDays = TimeUnit.MILLISECONDS.toDays(diff);

        String notifdate = null;
        if (diffSeconds == 0) {
            notifdate = "Now";
        }
        if (diffSeconds < 60 && diffSeconds > 1) {
            if (diffSeconds == 1) {
                notifdate = diffSeconds + " second ago";
            } else {
                notifdate = diffSeconds + " seconds ago";
            }
        } else if (diffSeconds > 60 && diffMinutes < 60) {
            if (diffMinutes == 1) {
                notifdate = diffMinutes + " minute ago";
            } else {
                notifdate = diffMinutes + " minutes ago";
            }
        } else if (diffMinutes > 60 && diffHours < 24) {
            if (diffHours == 1) {
                notifdate = diffHours + " hour ago";
            } else {
                notifdate = diffHours + " hours ago";
            }
        } else if (diffHours > 24 && diffDays < 29) {
            if (diffDays == 1) {
                notifdate = diffDays + " day ago";
            } else {
                notifdate = diffDays + " days ago";
            }
        } else if (diffDays > 29) {
            long month = 0;
            month = (diffDays / 30);

            if (month == 1) {
                notifdate = month + " month ago";
            } else {
                notifdate = month + " months ago";
            }
        } else if (diffDays > 355) {
            long year = 0;
            year = (diffDays / 356);

            if (year == 1) {
                notifdate = year + " year ago";
            } else {
                notifdate = year + " years ago";
            }
        }

        holder.duration.setText(notifdate);

        switch (dataModel.getType()) {
            case "bonus":
                holder.imageNotif.setImageDrawable(holder.mContext.getResources().getDrawable(R.mipmap.bonus_ic));
                break;
            case "promotion":
                holder.imageNotif.setImageDrawable(holder.mContext.getResources().getDrawable(R.mipmap.promo_ic));
                break;
            case "update":
                holder.imageNotif.setImageDrawable(holder.mContext.getResources().getDrawable(R.mipmap.update_ic));
                break;
            default:
                holder.imageNotif.setImageDrawable(holder.mContext.getResources().getDrawable(R.mipmap.news_ic));
                break;
        }

        final ViewHolder finalHolder = holder;
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DB dataBaseHelper = new DB(finalHolder.mContext);
                dataBaseHelper.openDatabase();

                dataBaseHelper.delete(dataModel.getNotifId());

                Toast.makeText(finalHolder.mContext, "Successfully removed", Toast.LENGTH_SHORT).show();

                notifyDataSetChanged();
                dataBaseHelper.close();

                BrowseActivity.Loaddb();

            }
        });

        return convertView;
    }

}