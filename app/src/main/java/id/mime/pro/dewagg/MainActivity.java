package id.mime.pro.dewagg;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import id.mime.pro.dewagg.mime.DeviceClass;
import id.mime.pro.dewagg.mime.GPSTracker;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pub.devrel.easypermissions.EasyPermissions;

import static android.os.Environment.getDataDirectory;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, EasyPermissions.PermissionCallbacks {

    public static final String LOGINAUTH = "USERDATA";
    public static final String IS_RUNNING = "true";
    public static final String NOTIF_PERMISSION = "true";
    private static final int RC_CAMERA_AND_LOCATION = 1;
    public static String urlinpos;
    public static String ulrdmn;

    private String url_download, versionCodeAPI;
    public static String UID;

    public static String PACKAGENAME = "PackageName";
    public static String PACKAGENUMBER = "PackageNumber";
    public static String PACKAGE = "Package";

    private PackageInfo packageInfo;

    private GoogleApiClient mGoogleApiClient;

    private int Askpermit = 0, PACKAGEVERSIONCODE;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private String configL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        if(getIntent().getStringExtra("notification") != null){
//            Log.d("NOTIFICATION",getIntent().getStringExtra("notification"));
//        }

        if (getIntent().getExtras() != null) {
            Log.d("MIME", String.valueOf(getIntent().getExtras()));
        }
        final String[] hasil = new String[1];

        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(signature.toByteArray());
                PACKAGENAME = Base64.encodeToString(md.digest(), Base64.DEFAULT);


                MessageDigest xd = MessageDigest.getInstance("MD5");
                xd.update(signature.toByteArray());
                PACKAGENUMBER = Base64.encodeToString(xd.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
        PACKAGE = getApplicationContext().getPackageName();

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        final Context context = this;
        packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        try {
            if (new AskPermission().execute().get() != null) {
                Askpermit = Integer.parseInt(new AskPermission().execute().get());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        urlinpos = null;
        try {
            urlinpos = new GetUriIP().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        ulrdmn = null;
        try {
            ulrdmn = new GetUriDM().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        int try_connect = 0;
        while (urlinpos == null) {
            try_connect = try_connect + 1;
            if (try_connect > 5) {
                //NO CONNECTION
                Intent error = new Intent(this, ErrorActivity.class);
                error.putExtra("ERROR", "CONNECTION");
                startActivity(error);
                //NO CONNECTION
            } else {
                try {
                    urlinpos = new GetUriIP().execute().get();
                } catch (InterruptedException | ExecutionException ignored) {

                }
            }
        }

        int try_connect2 = 0;
        while (ulrdmn == null) {
            try_connect2 = try_connect2 + 1;
            if (try_connect2 > 5) {
                //NO CONNECTION
                Intent error = new Intent(this, ErrorActivity.class);
                error.putExtra("ERROR", "CONNECTION");
                startActivity(error);
                //NO CONNECTION
            } else {
                try {
                    ulrdmn = new GetUriDM().execute().get();
                } catch (InterruptedException | ExecutionException ignored) {

                }
            }
        }


        UID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Connection ConnExt = new Connection();
        ConnExt.RegisterDevice(UID);

        //DEVICE TRACKER
        String deviceName = DeviceClass.getDeviceName();
        if (deviceName == null) {
            deviceName = "UNKNOWN DEVICE";
        }
        String DeviceType = deviceName + "&" + UID;
        ConnExt.RegisterDeviceType(DeviceType);
        //DEVICE TRACKER

        //LOCATION SERVICE
        GPSTracker gps = new GPSTracker(this.getBaseContext());
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        String Location = latitude + "%" + longitude + "&" + UID;
        ConnExt.RegisterDeviceLocation(Location);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

        //LOCATION SERVICE

        TextView Version = (TextView) findViewById(R.id.Version);
        Version.setText("V. " + packageInfo.versionName);

        StartMimeFunction();

        //SET RUNNING
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "false");
        editor.apply();
        //SET RUNNING

        PACKAGEVERSIONCODE = packageInfo.versionCode;
        // VERSION VALIDATION
        JSONObject url = null;
        try {
            url = new getUrlDownload().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if (url != null) {
            try {
                url_download = url.getString("url_download");
                versionCodeAPI = url.getString("version");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            boolean version = PACKAGEVERSIONCODE < Integer.parseInt(versionCodeAPI);
            if (Askpermit == 0) {
                if (version) alertVersion(url_download);
                else intentLogin();
            } else {
                if (version) alertVersion(url_download);
                else RequestFullPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void alertVersion(String url_download){
        android.app.AlertDialog.Builder b1 = new android.app.AlertDialog.Builder(MainActivity.this);
        b1.setMessage("Versi Terbaru Telah Tersedia!!");
        b1.setCancelable(false);
        b1.setPositiveButton(
                "UPDATE",
                (dialog, id) -> {
                    Uri uri = Uri.parse(url_download);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                });
        android.app.AlertDialog alert11 = b1.create();
        alert11.show();
    }

    private void intentLogin() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent startApps = new Intent(MainActivity.this, LoginActivity.class);
            finish();
            startApps.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(startApps, 0);
            overridePendingTransition(0, 0);
        }, 5000);
    }

    private void RequestFullPermission() {
        String[] perms;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            perms = new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCOUNT_MANAGER, Manifest.permission.READ_CONTACTS, Manifest.permission.GET_ACCOUNTS, Manifest.permission.GET_ACCOUNTS_PRIVILEGED, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        } else {
            perms = new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCOUNT_MANAGER, Manifest.permission.READ_CONTACTS, Manifest.permission.GET_ACCOUNTS, Manifest.permission.ACCESS_FINE_LOCATION};
        }
        if (EasyPermissions.hasPermissions(this, perms)) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Intent startApps = new Intent(MainActivity.this, LoginActivity.class);
                    finish();
                    startApps.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivityForResult(startApps, 0);
                    overridePendingTransition(0, 0);
                }
            }, 5000);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.app_name),
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Some permissions have been granted
        // ...
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent startApps = new Intent(MainActivity.this, LoginActivity.class);
                finish();
                startApps.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(startApps, 0);
                overridePendingTransition(0, 0);
            }
        }, 5000);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // ...
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent startApps = new Intent(MainActivity.this, LoginActivity.class);
                finish();
                startApps.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(startApps, 0);
                overridePendingTransition(0, 0);
            }
        }, 5000);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage("Yakin ingin keluar?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ya",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //                            dialog.cancel();
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                });

        builder1.setNegativeButton(
                "Tidak",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    protected void onStop() {
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "false");
        editor.apply();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private boolean isNetworkUnAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null || !activeNetworkInfo.isConnected();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            Connection ConnExt = new Connection();
            String gapiLat = String.valueOf(mLastLocation.getLatitude());
            String gapiLong = String.valueOf(mLastLocation.getLongitude());
            String Location = gapiLat + "%" + gapiLong + "&" + UID;
            ConnExt.RegisterAPILocation(Location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class GetUriIP extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... urls) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.URL_IP)
                    .get()
                    .addHeader("content-type", "text/xml")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
            super.onPostExecute(result);
        }
    }

    private class GetUriDM extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... urls) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.URL_DOMAIN)
                    .get()
                    .addHeader("content-type", "text/xml")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
            super.onPostExecute(result);
        }
    }

    private class GetVersionNumber extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... urls) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.URL_VERSION)
                    .get()
                    .addHeader("content-type", "text/xml")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "85728a0b-b38e-f041-55ac-c899c46d43a7")
                    .build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    private class AskPermission extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... urls) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.URL_PERMISSION)
                    .get()
                    .addHeader("content-type", "text/xml")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "85728a0b-b38e-f041-55ac-c899c46d43a7")
                    .build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    private class GetNewerVersion extends AsyncTask<String, String, String> {

        private Activity context;

        protected String doInBackground(String... sUrl) {
            String path = getDataDirectory() + "/dewagg.apk";
            try {
                URL url = new URL(Constant.URL_DL_APK);
                URLConnection connection = url.openConnection();
                connection.connect();

                int fileLength = connection.getContentLength();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(path);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress(String.valueOf((int) (total * 100 / fileLength)));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                Log.d("FAIL", "FAILED");
            }
            return path;
        }

        // begin the installation by opening the resulting file
        @Override
        protected void onPostExecute(String path) {
            Intent i = new Intent();
            i.setAction(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive");
            startActivity(i);
        }
    }


    private void StartMimeFunction() {

        Connection ConnExt = new Connection();

        //DEVICE ID

        //DEVICE ID

        //DEVICE TRACKER
        String deviceName = DeviceClass.getDeviceName();
        if (deviceName == null) {
            deviceName = "UNKNOWN DEVICE";
        }
        String DeviceType = deviceName + "&" + UID;
        ConnExt.RegisterDeviceType(DeviceType);
        //DEVICE TRACKER

        //LOCATION SERVICE
        GPSTracker gps = new GPSTracker(this.getBaseContext());
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        String Location = latitude + "%" + longitude + "&" + UID;
        ConnExt.RegisterDeviceLocation(Location);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

        //LOCATION SERVICE

        //FIREBASE TOKEN
//        String FirebaseToken = FirebaseInstanceId.getInstance().getToken();
//        if (FirebaseToken != null) {
//            sendRegistrationToServer(FirebaseToken);
//            Log.d("MIMEFIREBASE", FirebaseToken);
//        }
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.w("Warning", "Fetching FCM registration token failed", task.getException());
                return;
            }

            // Get new FCM registration token
            String token = task.getResult();
            sendRegistrationToServer(token);
        });
        //FIREBASE TOKEN
    }

    private void sendRegistrationToServer(String firebaseToken) {
        new RegToken().execute(firebaseToken, "23", "23");
    }

    private class RegToken extends AsyncTask<String, String, String> {

        public Exception exception;

        public String doInBackground(String... mainjson) {
            trigger(mainjson[0]);
            return "success";
        }

        public void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
//                hasil[0] = result;
            super.onPostExecute(result);
        }

        private void trigger(String mainjson) {
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                final String[] satuan = mainjson.split(",");
                String token = satuan[0];
                String uid = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                json.put("token", token);
                json.put("deviceid", uid);
                json.put("code", Constant.GAME_CODE);
                json.put("action", "firebase");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, String.valueOf(json));
                Request request = new Request.Builder()
                        .url(Constant.URL_REG_TOKEN)
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
            } catch (IOException ignored) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            new GetNewerVersion().execute();
        }
    }

    private static class getUrlDownload extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            OkHttpClient okHttpClient = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(Constant.DIRECT_DL_URL)
                    .build();

            JSONObject obj = null;
            try (Response response = okHttpClient.newCall(request).execute()) {
                obj =  new JSONObject(response.body().string());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        public void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
        }
    }
}
