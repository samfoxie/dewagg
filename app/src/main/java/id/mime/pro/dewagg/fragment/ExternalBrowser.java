package id.mime.pro.dewagg.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableWebView;
import com.google.android.gms.analytics.Tracker;

import id.mime.pro.dewagg.MainActivity;
import id.mime.pro.dewagg.R;

import static android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;
import static id.mime.pro.dewagg.MainActivity.IS_RUNNING;


/**
 * Created by pc on 4/25/2017.
 */

public class ExternalBrowser extends AppCompatActivity {

    private TextView page_title;
    private ImageView BACK_BUTTON;

    private myWebChromeClient mWebChromeClient;
    private ObservableWebView mWebView;
    private int isGame = 0;

    private int v88 = 0;

    private RelativeLayout overlay;

    private Tracker mTracker;

    private boolean isNetworkUnAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null || !activeNetworkInfo.isConnected();
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    private static final String TAG = MainActivity.class.getSimpleName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.external_browser);

        page_title = (TextView) findViewById(R.id.page_title);
        BACK_BUTTON = (ImageView) findViewById(R.id.back_button);

        BACK_BUTTON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Intent myIntent = getIntent();
        String url = myIntent.getStringExtra("url");
        mWebView = null;
        mWebView = (ObservableWebView) findViewById(R.id.webView);
        final WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setUserAgentString(getSharedPreferences("external_data", 0).getString("user_agent", ""));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        }
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setSupportMultipleWindows(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webSettings.setDomStorageEnabled(true);
        mWebChromeClient = new myWebChromeClient();
        mWebView.setWebChromeClient(mWebChromeClient);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        mWebView.setDownloadListener(new DownloadListener() {

            public void onDownloadStart(final String url, String userAgent,
                                        final String contentDisposition, final String mimetype,
                                        long contentLength) {
                verifyStoragePermissions(ExternalBrowser.this);
                registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

                final String filename = URLUtil.guessFileName(url, contentDisposition, mimetype);
                Snackbar snackbar = Snackbar
                        .make(mWebView, getString(R.string.toast_download_1) + " " + filename, Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.toast_yes), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                                request.addRequestHeader("Cookie", CookieManager.getInstance().getCookie(url));
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
                                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                                dm.enqueue(request);
                                Snackbar.make(mWebView, getString(R.string.toast_download) + " " + filename, Snackbar.LENGTH_SHORT).show();
                            }
                        });
                snackbar.show();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar4);
        overlay = (RelativeLayout) findViewById(R.id.overlay);
        if (Uri.parse(url).getHost().equals("www.jualv88.com") || Uri.parse(url).getHost().equals("jualv88.com")) {
            if (v88 == 0) {
                url = "http://store.bisasukses.com/embedcart?site_key=akSf8BgvYc1wsnmupJoeAHD0RXI4U23VFO6WEy9M&callback=http%3A%2F%2Fwww.jualv88.com%2F&width=100%&theme=light&urlvars=&direct_app=true";
                v88 = 1;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setPackage("com.android.chrome");
            try {
                getApplicationContext().startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                // Chrome browser presumably not installed so allow user to choose instead
                intent.setPackage(null);
                getApplicationContext().startActivity(intent);
            }
            finish();

        }
        if (Uri.parse(url).getHost().equals("ib.bankmandiri.co.id") || Uri.parse(url).getHost().equals("ibank.bni.co.id") || Uri.parse(url).getHost().equals("ibank.klikbca.com") || Uri.parse(url).getHost().equals("ib.bri.co.id")) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            isGame = 0;
        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            isGame = 1;
            toolbar.setVisibility(View.GONE);
            overlay.setVisibility(View.VISIBLE);
        }
        Log.d("CONFIGURATION", url);
        mWebView.loadUrl(url);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                return false;
            }
        });


//        int v = getResources().getConfiguration().orientation;
//        if(v == 2){
//            toolbar.setVisibility(View.GONE);
//        }
//        else {
//            toolbar.setVisibility(View.VISIBLE);
//        }

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar4);
        // Checks the orientation of the screen
        if (isGame == 1) {
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                toolbar.setVisibility(View.GONE);
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                toolbar.setVisibility(View.GONE);
            }
        } else {
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                toolbar.setVisibility(View.GONE);
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                toolbar.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    protected void onStop() {
//        unregisterReceiver(MessageReceiver);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences Running;
        Running = getSharedPreferences(IS_RUNNING, MODE_PRIVATE);
        SharedPreferences.Editor editor = Running.edit();
        editor.putString("RUNNING", "false");
        editor.apply();
    }

    @Override
    public void onBackPressed() {
        if (isGame == 0) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }

    private final BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Snackbar snackbar = Snackbar
                    .make(mWebView, getString(R.string.toast_download_2), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.toast_yes), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
                        }
                    });
            snackbar.show();
            unregisterReceiver(onComplete);
        }
    };


    private class myWebChromeClient extends WebChromeClient {

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            mWebView.removeAllViews();
            WebView newView = new WebView(getApplicationContext());
            newView.setWebViewClient(new WebViewClient());
            // Create dynamically a new view
            newView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            mWebView.addView(newView);

            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newView);
            resultMsg.sendToTarget();
            return true;
        }


        @Override
        public void onGeolocationPermissionsShowPrompt(final String origin, final GeolocationPermissions.Callback callback) {

            final boolean remember = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(ExternalBrowser.this);
            AlertDialog alert = builder.create();
            alert.show();
        }

        public void onProgressChanged(WebView view, int progress) {
            if (progress == 100) {
                page_title.setText(mWebView.getTitle());
                overlay.setVisibility(View.GONE);
            } else {
                page_title.setText("Loading..");
            }
        }
    }

    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


}
