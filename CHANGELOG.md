## 2.0.0 (September 09, 2020)
  - Merge branch 'feat-dynamic-url' into develop
  - Update versionName and versionCode
  - Update version validation
  - Implement END POINT API and LiveChat libs
  - Remove feature download APK
  - Remove MimeFunction
  - Remove unused file
  - Update gitignore
  - Add bump-version.sh
  - Merge branch 'pepe' into develop
  - Update
  - Clear Hardcoded
  - Merge branch 'fix-download-apk' into develop
  - Fix Download Apk on WebView
  - Update gitignore
  - First commit.

